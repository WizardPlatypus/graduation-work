﻿using System;
using System.Collections.Generic;
using Algorithms.Datastructures;

using FindCycles = Algorithms.FindAllCyclesGraph;

namespace Algorithms {
    /// <summary>
    /// Two salesmen algorithm
    /// </summary>
    public class TwoSalesmen : IAlgorithm {
        #region Algorithm
        public void Start() {
            // Етап 1: Пошук циклів
            cycles = (Perform<FindCycles>.On(new FindCycles.Input(new Graph(weightedMatrix.AsMatrix), targetVertex)) as FindCycles.Result).Cycles;
            // Етап 2: Обчислання ваги циклів
            weights = (Perform<WeightCycles>.On(new WeightCycles.Input(cycles, weightedMatrix)) as WeightCycles.Result).Weights;
            // Етап 3: Обчислення використаних циклами вершин
            used = new List<int>(cycles.Count);
            foreach (var cycle in cycles) {
                used.Add(GetUsedFrom(cycle));
            }
            { // Етап 4: Пошук найкращої комбінації циклів
                int currentSum, currentMin, singleWeight;
                // загальна сума найкращої комбінації
                currentSum = int.MaxValue;
                // загальний час найкращої комбінації
                currentMin = int.MaxValue;
                // час руху одного комівояжера
                singleWeight = int.MaxValue;
                // стан набору вершин, коли усі вершини
                // у графі відвідано
                int full = (1 << weightedMatrix.Size) - 1;
                // перебір можливих комбінацій
                for (int i = 0; i < cycles.Count; i++) {
                    // у випадку, коли i-ий цикл проходить через усі вершини
                    if (used[i] == full) {
                        // пошук найкращого циклу для одного комівояжера
                        if (weights[i] < singleWeight) {
                            singleWeight = weights[i];
                            singleCycle = i;
                        }
                        // пошук найкращого циклу для двох комівояжерів
                        if (weights[i] < currentMin) {
                            cycle1 = cycle2 = i;
                            currentMin = currentSum = weights[i];
                        }
                        // немає сенсу шукати комбінації з i-им циклом:
                        // набір відвіданих вершин не змінюватиметься
                        continue;
                    }
                    // підбір другого циклу у комбінації
                    for (int j = i + 1; j < cycles.Count; j++) {
                        // аналогічно розглядається випадок, коли j-ий
                        // цикл проходить через усі вершини у графі
                        if (used[j] == full) {
                            if (weights[j] < singleWeight) {
                                singleWeight = weights[j];
                                singleCycle = j;
                            }
                            if (weights[j] < currentMin) {
                                cycle1 = cycle2 = j;
                                currentMin = currentSum = weights[j];
                            }
                            continue;
                        }
                        // якщо не всі вершини відвідані комбінацією
                        // двох циклів, комбінація не підходить
                        if ((used[i] | used[j]) != full)
                            continue;
                        // загальний час нової комбінації
                        var newMin = Math.Max(weights[i], weights[j]);
                        // загальна сума нової комбінації
                        var newSum = weights[i] + weights[j];
                        // якщо нова комбінація за параметрами
                        // загального часу та суми краща за
                        // "найкращу" комбінацію
                        if (currentMin > newMin ||
                            (currentMin == newMin
                            && currentSum > newSum)) {
                            cycle1 = i;
                            cycle2 = j;
                            currentMin = newMin;
                            currentSum = newSum;
                        }
                    }
                }
                // якщо цикли рівні, вигідніше використовувати
                // тільки одного комівояжера
                if (cycle1 == cycle2) {
                    cycle2 = -1;
                }
            }
        }
        int GetUsedFrom(List<int> cycle) {
            // бітова логіка у С# має обмеження: вона реалізована
            // тільки для типу int. отже ефективно працювати можна
            // тільки з sizeof(int) * 8 вершинами — по біту на
            // кожну вершину.
            if (weightedMatrix.Size > sizeof(int) * 8) {
                throw new OutOfMemoryException("Too many vertices in graph, can't efficently store used values of cycles");
            }
            // змінна u позначатиме набір вершин у циклі
            // перед початком обчислення у наборі нема вершин
            int u = 0;
            foreach (var vertex in cycle) {
                // помічаємо біт у позиції vertex
                u |= 1 << vertex;
            }
            return u;
        }
        #endregion

        #region Public member classes
        public class Input : IAlgorithm.Input {
            public WeightedMatrix WeightedMatrix { get; set; }
            public int TargetVertex { get; set; }
            public Input(WeightedMatrix matrix, int targetVertex) {
                WeightedMatrix = matrix;
                TargetVertex = targetVertex;
            }
        }
        public class Result : IAlgorithm.Result {
            public List<int> Cycle1 { get; set; }
            public List<int> Cycle2 { get; set; }
            public List<int> SingleCycle { get; set; }
            public int Index1 { get; set; }
            public int Index2 { get; set; }
            public int SingleIndex { get; set; }
            public int Weight1 { get; set; }
            public int Weight2 { get; set; }
            public int SingleWeight { get; set; }
            public int TotalWeight { get => Math.Max(Weight1, Weight2); }
            public List<List<int>> AllCycles { get; set; }
            public List<int> Weights { get; set; }
            public List<int> Used { get; set; }
        }
        #endregion

        #region Input/Result
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as Input;
            weightedMatrix = _input.WeightedMatrix;
            targetVertex = _input.TargetVertex;
        }
        public IAlgorithm.Result GetResult() {
            return new Result() {
                AllCycles = cycles,
                Cycle1 = cycles[cycle1],
                Cycle2 = cycle2 == -1 ? null : cycles[cycle2],
                SingleCycle = singleCycle == -1 ? null : cycles[singleCycle],
                Index1 = cycle1,
                Index2 = cycle2,
                SingleIndex = singleCycle,
                Weight1 = weights[cycle1],
                Weight2 = cycle2 == -1 ? 0 : weights[cycle2],
                SingleWeight = singleCycle == -1 ? 0 : weights[singleCycle],
                Used = used,
                Weights = weights
            };
        }
        #endregion Input/Result

        #region Private Fields
        private WeightedMatrix weightedMatrix;
        private int targetVertex;
        private List<List<int>> cycles;
        private List<int> weights;
        private List<int> used;
        private int cycle1 = -1, cycle2 = -1, singleCycle = -1;
        #endregion
    }
}
