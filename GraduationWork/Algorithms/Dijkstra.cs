﻿using System;
using System.Collections.Generic;
using Algorithms.Datastructures;

namespace Algorithms {
    public class Dijkstra : IAlgorithm {
        public void Start() {
            InitializeFields();
            DoDijkstra();
        }
        private void InitializeFields() {
            // distances
            distances = new int[matrix.Size];
            for (int i = 0; i < distances.Length; ++i) {
                // default value of distance must be
                // infinity (in this case, nothing is bigger than
                // int.MaxValue, so it works)
                distances[i] = int.MaxValue;
            }
            distances[start_vertex] = 0; // distance to start vertex is 0
            // ancestors
            ancestors = new int[matrix.Size];
            ancestors[start_vertex] = -1;
            // sequence
            sequence = new List<int>();
            sequence.Capacity = matrix.Size;
            // visits
            visits = new bool[matrix.Size];
        }

        #region Algorithm
        private void DoDijkstra() {
            int from; // current point
            while (true) {
                if ((from = GetNewFrom()) == -1) { // if can't find new victim
                    break;
                }
                visits[from] = true;
                for (int to = 0; to < matrix.Size; ++to) {
                    var edge = matrix[from, to];
                    if (edge == 0)
                        continue; // skip points which don't have a common edge
                    var possible_distance = distances[from] + edge;
                    if (possible_distance < distances[to]) {
                        ancestors[to] = from;
                        distances[to] = possible_distance;
                    }
                }
            }
        }
        private int GetNewFrom() {
            int from = -1;
            // searching unvisited point to take it as default from
            for (int vertex = 0; vertex < matrix.Size; ++vertex)
            //(technically there is no need for i variable)
            {
                if (visits[vertex]) // skip visited points
                    continue;
                // take the first unvisited point
                from = vertex;
                break;
            }
            if (from == -1) { // if no unvisited points left
                return -1;
            }
            for (int vertex = from + 1; vertex < matrix.Size; ++vertex) {
                if (visits[vertex]) // skip visited point
                    continue;
                if (distances[vertex] < distances[from])
                    from = vertex; // take point with shorter distance
            }
            sequence.Add(from);
            return from;
        }
        #endregion

        #region Input/Result
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as Dijkstra.Input;
            if (_input == null)
                throw new ArgumentNullException();
            matrix = _input.Matrix;
            start_vertex = _input.StartVertex;
        }
        public IAlgorithm.Result GetResult()
            => new Dijkstra.Result(distances, sequence, ancestors);
        #endregion

        #region Public member classes
        public class Input : IAlgorithm.Input {
            private WeightedMatrix matrix;
            private int start_vertex;
            public Input(WeightedMatrix matrix, int start_vertex) {
                this.matrix = matrix;
                this.start_vertex = start_vertex;
            }
            public WeightedMatrix Matrix { get => matrix; }
            public int StartVertex { get => start_vertex; }
        }
        public class Result : IAlgorithm.Result {
            int[] distances;
            List<int> sequence;
            int[] ancestors;
            public int[] Distances { get => distances; }
            public List<int> Sequence { get => sequence; }
            public int[] Ancestors { get => ancestors; }
            public Result(int[] distances, List<int> sequence, int[] ancestors) {
                this.distances = distances;
                this.sequence = sequence;
                this.ancestors = ancestors;
            }
        }
        #endregion

        #region Private fields
        private WeightedMatrix matrix;
        private bool[] visits;
        private int start_vertex;
        private int[] distances;
        private List<int> sequence;
        private int[] ancestors;
        #endregion
    }
}