﻿using System.Collections.Generic;
using Algorithms.Datastructures;

namespace Algorithms {
    /// <summary>
    /// Algorithm to calculate weight of cycles in given weighted matrix
    /// </summary>
    public class WeightCycles : IAlgorithm {
        public void Start() {
            weights = new List<int>(cycles.Count);
            foreach (var cycle in cycles) {
                int begin, end, weight = 0;
                for (int i = 0; i < cycle.Count - 1; ++i) {
                    begin = cycle[i];
                    end = cycle[i + 1];
                    weight += matrix[begin, end];
                }
                weights.Add(weight);
            }
        }

        #region Public member classes
        public class Input : IAlgorithm.Input {
            public List<List<int>> Cycles { get; set; }
            public WeightedMatrix WeightedMatrix { get; set; }
            public Input(List<List<int>> cycles, WeightedMatrix weightedMatrix) {
                Cycles = cycles;
                WeightedMatrix = weightedMatrix;
            }
        }
        public class Result : IAlgorithm.Result {
            public List<int> Weights { get; set; }
            public Result(List<int> weights) {
                Weights = weights;
            }
        }
        #endregion

        #region Input/Result
        public IAlgorithm.Result GetResult() {
            return new Result(weights);
        }
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as Input;
            cycles = _input.Cycles;
            matrix = _input.WeightedMatrix;
        }
        #endregion Input/Result

        #region Private fields
        private List<List<int>> cycles;
        private WeightedMatrix matrix;
        private List<int> weights;
        #endregion
    }
}
