﻿using System;
using System.Collections.Generic;
using Algorithms.Datastructures;

namespace Algorithms {
    /// <summary>
    /// Depth First Search Algorithm
    /// </summary>
    public class DFS : IAlgorithm {
        public void Start() {
            sequence = new List<int>();
            for (int victim = 0; victim < graph.Order; ++victim) {
                if (!visited[victim])
                    Search(victim);
            }
        }

        #region Algorithm
        virtual protected void Search(int current, int depth = 0) {
            sequence.Add(current);
            visited[current] = true;
            if (depth > maxDepth)
                maxDepth = depth;
            foreach (int victim in graph.AsIncidencyLists[current]) {
                if (!visited[victim]) {
                    Search(victim, depth + 1);
                }
            }
        }
        #endregion

        #region Input/Result
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as DFS.Input;
            if (_input.Graph == null)
                throw new ArgumentNullException();
            graph = _input.Graph;
            visited = new bool[graph.Order];
        }
        public IAlgorithm.Result GetResult()
            => new DFS.Result(maxDepth, sequence.ToArray());
        #endregion

        #region Private fields
        protected bool[] visited;
        protected Graph graph;
        protected int maxDepth;
        protected List<int> sequence;
        #endregion

        #region Public member classes
        public class Input : IAlgorithm.Input {
            private Graph graph;
            public Input(IncidencyLists incidencyLists) {
                graph = new Graph(incidencyLists);
            }
            public Input(AdjacencyMatrix incidencyMatrix) {
                graph = new Graph(incidencyMatrix);
            }
            public Input(Graph graph) {
                this.graph = graph;
            }

            public Graph Graph { get => graph; set => graph = value; }
        }
        public class Result : IAlgorithm.Result {
            public int MaxDepth { get; set; }
            public int[] Sequence { get; set; }
            public Result(int maxDepth, int[] sequence) {
                MaxDepth = maxDepth;
                Sequence = sequence;
            }
        }
        #endregion
    }
    public class RecDFSbyIncidencyMatrix : DFS {
        override protected void Search(int current, int depth = 0) {
            sequence.Add(current);
            visited[current] = true;
            if (depth > maxDepth)
                maxDepth = depth;
            for (int victim = 0; victim < graph.Order; ++victim) {
                bool IsIncident = graph.AsIncidencyMatrix[current, victim] == true;
                bool IsUnvisited = !visited[victim];
                if (IsIncident && IsUnvisited)
                    Search(victim, depth + 1);
            }
        }
    }
    public class RecDFSbyIncidencyLists : DFS {
        protected override void Search(int current, int depth = 0) {
            sequence.Add(current);
            visited[current] = true;
            if (depth > maxDepth)
                maxDepth = depth;
            foreach (int victim in graph.AsIncidencyLists[current]) {
                if (!visited[victim]) {
                    Search(victim, depth + 1);
                }
            }
        }
    }
    public class NonRecDFSbyIncidencyMatrix : DFS {
        protected override void Search(int current, int depth = 0) {
            Stack<int> stack = new Stack<int>();
            visited[current] = true;
            stack.Push(current);
            sequence.Add(current);
            while (stack.Count != 0) {
                current = stack.Peek();
                if (stack.Count - 1 > maxDepth)
                    maxDepth = stack.Count - 1;
                int victim = -1;
                for (int i = 0; i < graph.Order; ++i) {
                    bool IsIncident = graph.AsIncidencyMatrix[current, i];
                    bool IsUnvisited = !visited[i];
                    if (IsIncident && IsUnvisited) {
                        victim = i;
                        break;
                    }
                }
                if (victim == -1) {
                    stack.Pop();
                    continue;
                }
                visited[victim] = true;
                stack.Push(victim);
                sequence.Add(victim);
            }
        }
    }
    public class NonRecDFSbyIncidencyLists : DFS {
        protected override void Search(int current, int depth = 0) {
            Stack<int> stack = new Stack<int>();
            visited[current] = true;
            stack.Push(current);
            sequence.Add(current);
            while (stack.Count != 0) {
                current = stack.Peek();
                if (stack.Count - 1 > maxDepth)
                    maxDepth = stack.Count - 1;
                int victim = -1;
                foreach (int i in graph.AsIncidencyLists[current]) {
                    if (!visited[i]) {
                        victim = i;
                        break;
                    }
                }
                if (victim == -1) {
                    stack.Pop();
                    continue;
                }
                visited[victim] = true;
                stack.Push(victim);
                sequence.Add(victim);
            }
        }
    }
}
