﻿


//!!! DEPRECATED !!!//



using System.Collections.Generic;
using Algorithms.Datastructures;

namespace Algorithms.Multigraph {
    /// <summary>
    /// Implements graph to multimatrix convertion
    /// </summary>
    public class GraphToMultimatrix : IAlgorithm {
        #region Input/Result
        public class Input : IAlgorithm.Input {
            public Graph Graph { get; set; }
            public Input(Graph graph) {
                Graph = graph;
            }
        }
        public class Result : IAlgorithm.Result {
            public List<List<int>> Edges { get; set; }
            public Multimatrix Multimatrix { get; set; }
            public int[] Mapping { get; set; }
            public Result(Multimatrix multimatrix, List<List<int>> edges, int[] mapping) {
                Multimatrix = multimatrix;
                Edges = edges;
                Mapping = mapping;
            }
        }
        public void SetInput(IAlgorithm.Input input) {
            graph = (input as Input).Graph;
        }
        public IAlgorithm.Result GetResult() {
            return new Result(multimatrix, edges, mapping);
        }
        #endregion

        Graph graph;
        Multimatrix multimatrix;
        List<List<int>> edges;
        int[] mapped;
        int[] mapping;
        int[] degrees;

        public void Start() {
            mapped = new int[graph.Order];
            degrees = graph.GetDegrees();

            // figuring out size of multigraph, mapping vertices
            int count = 0;
            for (int vertex = 0; vertex < graph.Order; ++vertex) {
                if (degrees[vertex] == 2) {
                    mapped[vertex] = -1; // this vertex will be inner
                } else {
                    mapped[vertex] = count++; // this vertex will
                    // have index equal to a number of previously
                    // indexed vertices
                }
            }

            multimatrix = new Multimatrix(order: count, size: 0);
            mapping = CalculateMapping();
            edges = new List<List<int>>();
            for (int vertex = 0; vertex < graph.Order; ++vertex) {
                if (mapped[vertex] < 0)
                    continue;
                foreach (int incident in graph.AsIncidencyLists[vertex]) {
                    // if incident was already used, skip
                    if (mapped[incident] == -2)
                        continue;
                    var edge = new List<int>();
                    var end = FindEnd(vertex, incident, edge);
                    if (multimatrix[mapped[vertex], mapped[end]] == null)
                        multimatrix[mapped[vertex], mapped[end]] = new List<int>();
                    // new edge will have an index equal to the number of
                    // edges previously calculated
                    multimatrix[mapped[vertex], mapped[end]].Add(edges.Count);
                    edges.Add(edge);
                }
                mapped[vertex] = -2;
            }
            multimatrix.Size = edges.Count;
        }
        int FindEnd(in int previous, in int current, List<int> edge) {
            if (degrees[current] != 2)
                return current;
            edge.Add(current);
            mapped[current] = -2;
            var relative1 = graph.AsIncidencyLists[current][0];
            var relative2 = graph.AsIncidencyLists[current][1];
            var next = relative1 == previous ? relative2 : relative1;
            return FindEnd(current, next, edge);
        }
        int[] CalculateMapping() {
            int[] mapping = new int[multimatrix.Order];
            for (int i = 0; i < mapped.Length; ++i) {
                if (mapped[i] < 0)
                    continue;
                mapping[mapped[i]] = i;
            }
            return mapping;
        }
    }
}
