﻿


// !!! Deprecated !!!



using System.Collections.Generic;

namespace Algorithms.Multigraph {
    public class Multimatrix {
        List<int>[,] multimatrix;
        int order, size;
        public Multimatrix(List<int>[,] matrix, int order, int size) {
            this.multimatrix = matrix;
            this.order = order;
            this.size = size;
        }
        public Multimatrix(int order, int size) {
            multimatrix = new List<int>[order, order];
            this.order = order;
            this.size = size;
        }

        #region Public properties & indexers
        /// <summary>
        /// Order of represented graph.
        /// </summary>
        public int Order { get => order; }
        /// <summary>
        /// Number of edges in multigraph.
        /// </summary>
        public int Size { get => size; set => size = value; }
        /// <summary>
        /// if multimatrix[i,j] == null i,j are not adjacent,
        /// otherwise multimatrix[i,j] gives access to an array of
        /// edges, each edge might have some inner vertices.
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        public List<int> this[int i, int j] {
            get => multimatrix[i, j];
            set => multimatrix[i, j] = multimatrix[j, i] = value;
        }
        #endregion
    }
}
