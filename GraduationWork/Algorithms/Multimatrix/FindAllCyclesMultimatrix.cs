﻿


//!!! DEPRECATED !!!//



using System;
using System.Collections.Generic;
using System.Linq;

namespace Algorithms.Multigraph {
    public class FindAllCyclesMultimatrix : IAlgorithm {
        public void Start() {
            cycles = new List<int[]>();
            var visited = new bool[multimatrix.Size];
            var cycle = new int[] { start_vertex };
            for (int vertex = 0; vertex < multimatrix.Order; ++vertex) {
                if (multimatrix[start_vertex, vertex] == null)
                    continue; // vertex is not incident
                foreach (var edge in multimatrix[start_vertex, vertex]) {
                    Search(vertex, Added(cycle, edge), MarkedAt(visited, edge), edge);
                }
            }
        }

        #region Algorithm
        //!!! TODO: refactor the search with backtracking
        /// <summary>
        /// Performs a DFS search on multimatrix in desire to find all cycles of multimatrix
        /// </summary>
        private void Search(int current, int[] cycle, bool[] visited, int first_edge) {
            cycle = Added(cycle, current);
            if (current == start_vertex) {
                cycles.Add(cycle);
                return;
            }
            for (int vertex = 0; vertex < multimatrix.Order; ++vertex) {
                if (multimatrix[current, vertex] == null)
                    continue; // vertex is not incident
                foreach (var edge in multimatrix[current, vertex]) {
                    if (visited[edge])
                        continue; // edge was used in this cycle
                    if (vertex == start_vertex && edge < first_edge)
                        continue;
                    Search(vertex, Added(cycle, edge), MarkedAt(visited, edge), first_edge);
                }
            }
        }
        /// <summary>
        /// Creates deep copy of given array and adds new element to
        /// it.
        /// </summary>
        /// <param name="array">array to be copied</param>
        /// <param name="element">element to be added</param>
        /// <returns>new array, containing given array and element</returns>
        int[] Added(int[] array, int element) {
            int[] new_array = new int[array.Length + 1];
            Array.Copy(array, new_array, array.Length);
            new_array[new_array.Length - 1] = element;
            return new_array;
        }
        /// <summary>
        /// Creates deep copy of given array and marks element at
        /// specified position
        /// </summary>
        /// <param name="array">array to be copied</param>
        /// <param name="position">position to be marked</param>
        /// <returns></returns>
        bool[] MarkedAt(bool[] array, int position) {
            bool[] new_array = new bool[array.Length];
            Array.Copy(array, new_array, array.Length);
            new_array[position] = true;
            return new_array;
        }
        #endregion

        #region Input/Result
        public IAlgorithm.Result GetResult() {
            List<List<int>> _cycles = new List<List<int>>();
            foreach (var cycle in cycles) {
                _cycles.Add(cycle.ToList());
            }
            return new Result(_cycles);
        }
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as Input;
            multimatrix = _input.Multimatrix;
            start_vertex = _input.StartVertex;
        }
        #endregion Input/Result

        #region Private fields
        private Multimatrix multimatrix;
        private int start_vertex;
        private List<int[]> cycles;
        #endregion

        #region Public member classes
        public class Input : IAlgorithm.Input {
            public Multimatrix Multimatrix { get; set; }
            public int StartVertex { get; set; }
            public Input(Multimatrix multimatrix, int start_vertex) {
                Multimatrix = multimatrix;
                StartVertex = start_vertex;
            }
        }
        public class Result : IAlgorithm.Result {
            public List<List<int>> Cycles { get; set; }
            public Result(List<List<int>> cycles) {
                Cycles = cycles;
            }
        }
        #endregion
    }
}
