﻿using System;
using System.Collections.Generic;

namespace Algorithms.Datastructures {
    /// <summary>
    /// Interface for N by N matrix with cells of type T.
    /// </summary>
    /// <typeparam name="T">type of cells</typeparam>
    public interface IMatrix<T> {
        #region Size flexibility
        public void Grow(int growth);
        public void Delete(int vertex);
        #endregion

        #region Properties & indexers
        public T this[int i, int j] { get; set; }
        public int Size { get; }
        public List<T> GetValues();
        #endregion
    }

    /// <summary>
    /// Represents N by N matrix of cells, such that:
    /// 1. Each cell is of type T.
    /// 2. Cells are accesed by two indices, i and j, aka [i, j].
    /// 3. Default value of type T denotes inexistence of a particular cell, aka if matrix[i,j] == default_of(T), then cell [i, j] does not exist.
    /// 4. For any i, matrix[i,i] is always set to default_of(T).
    /// 5. matrix[i, j] is always equal to matrix[j, i].
    /// All cells of the matrix are contained in one-dimensional array,
    /// so that's why it's called FlatMatrix.
    /// Time Complexities:
    /// Grow(int growth) — O(size^2), more precisely (size + growth + size*(size - 1)/2)
    /// Delete(int vertex) — O(size^2), more precisely size*(size - 1)/2
    /// Access — O(1)
    /// GetValues — O(1)
    /// Space complexity: O(size^2), but actually exactly size*(size - 1)/2 elements are stored
    /// </summary>
    /// <typeparam name="T">Type of cells in matrix</typeparam>
    public class FlatMatrix<T> : IMatrix<T> {
        #region Constructors
        public FlatMatrix(int size = 0) {
            this.size = size;
            var len = CalculateLengthFor(size);
            values = new List<T>(len);
            for (int i = 0; i < len; i++) values.Add(_default);
        }
        public FlatMatrix(T[,] source, int size)
            : this(size) {
            for (int i = 0; i < size; i++) {
                for (int j = i + 1; j < size; j++) {
                    values[Index(i, j)] = source[i, j];
                }
            }
        }
        public FlatMatrix(int size, T[,] source)
            : this(source, size) { }
        #endregion

        #region Size flexibility
        public void Grow(int growth) {
            int old_size = size;
            size += growth;
            values.AddRange(new T[CalculateLengthFor(size) - values.Count]);
            for (int i = old_size - 1; i >= 0; i--) {
                for (int j = old_size - 1; j > i; j--) {
                    if (i == j) continue;
                    int index, old_index;
                    index = GetIndex(i, j, size);
                    old_index = GetIndex(i, j, old_size);
                    { // swap
                        var t = values[index];
                        values[index] = values[old_index];
                        values[old_index] = t;
                    }
                }
            }
        }
        public void Delete(int vertex) {
            int[] del = new int[size - -1];
            int del_i = 0;
            for (int i = 0; i < vertex; i++) {
                del[del_i++] = Index(vertex, i);
            }
            for (int i = vertex + 1; i < size; i++) {
                del[del_i++] = Index(vertex, i);
            }
            List<T> new_values = new List<T>(values.Count - size + 1);
            del_i = 0;
            for (int i = 0; i < values.Count; i++) {
                if (del_i < del.Length && i == del[del_i]) {
                    del_i++;
                    continue;
                }
                new_values.Add(values[i]);
            }
            size--;
            values = new_values;
        }
        #endregion

        #region Properties & Indexers
        private int Index(int i, int j)
            => GetIndex(i, j, size);
        public T this[int i, int j] {
            get {
                if (i == j)
                    return _default;
                int index = Index(i, j);
                return values[index];
            }
            set {
                if (i == j)
                    return;
                int index = Index(i, j);
                values[index] = value;
            }
        }
        public int Size { get => size; }
        public List<T> GetValues() => values;
        #endregion

        #region Static methods
        private static int GetIndex(int i, int j, int size) {
            if (i == j)
                throw new ArgumentException($"i and j must not be same. i = {i}, j = {j}");
            if (i > j)
                (i, j) = (j, i);
            return i * size - ((i + 1) * (i + 2) / 2) + j;
        }
        private static int CalculateLengthFor(int size) {
            return 1 + GetIndex(size - 2, size - 1, size);
        }
        #endregion

        #region Private fields
        private T _default;
        private List<T> values;
        private int size;
        #endregion
    }

    /// <summary>
    /// Represents N by N matrix of cells, such that:
    /// 1. Each cell is of type T.
    /// 2. Cells are accesed by two indices, i and j, aka [i, j].
    /// 3. Default value of type T denotes inexistence of a particular cell, aka if matrix[i,j] == default_of(T), then cell [i, j] does not exist.
    /// 4. For any i, matrix[i,i] is always set to default_of(T).
    /// 5. matrix[i, j] is always equal to matrix[j, i].
    /// Cells of the matrix are stored as a sequence of arrays of different lengths,
    /// so that's why it's called JaggedMatrix
    /// Time Complexities:
    /// Grow(int growth) — O(size^2 + growth^2), more precisely (size*(size + growth) +growth^2)
    /// Delete(int vertex) — O(vertex^3), more precisely (vertex^2*(vertex+1)/2)
    /// Access — O(1)
    /// GetValues — O(size^2), more precisely(size*(size - 1)/2),
    /// althouh it's lazy, meaning it will cost O(1) to call GetValues()
    /// on unchanged structure any time but first.
    /// Space complexity: O(size^2), but actually exactly size*(size - 1)/2 elements are stored
    /// </summary>
    /// <typeparam name="T">Type of cells in matrix</typeparam>
    public class JaggedMatrix<T> : IMatrix<T> {
        #region Constructors
        public JaggedMatrix(int size = 0) {
            this.size = size;
            matrix = new List<List<T>>();
            for (int i = 0; i < size; i++) {
                var list = new List<T>(size - i - 1);
                list.AddRange(new T[size - i - 1]);
                matrix.Add(list);
            }
            _values = null;
        }
        public JaggedMatrix(int size, T[,] source)
            : this(size) {
            for (int i = 0; i < size; i++) {
                for (int j = i + 1; j < size; j++) {
                    matrix[i][j - i - 1] = source[i, j];
                }
            }
        }
        #endregion

        #region Size flexibility
        public void Grow(int growth) {
            foreach (var list in matrix) {
                list.AddRange(new T[growth]);
            }
            for (int i = 0; i < growth; i++) {
                var list = new List<T>(growth - i - 1);
                list.AddRange(new T[growth - i - 1]);
                matrix.Add(list);
            }
            size += growth;
            _values = null;
        }
        public void Delete(int vertex) {
            matrix.RemoveAt(vertex);
            for (int i = 0; i < vertex; i++) {
                matrix[i].RemoveAt(vertex - i - 1);
            }
            _values = null;
        }
        #endregion

        #region Properties & indexers
        public T this[int i, int j] {
            get {
                if (i > j) (i, j) = (j, i);
                if (i >= size - 1 || j >= size || i == j)
                    return _default;
                return matrix[i][j - i - 1];
            }
            set {
                if (i > j) (i, j) = (j, i);
                if (i >= size - 1 || j >= size || i == j)
                    throw new IndexOutOfRangeException();
                matrix[i][j - i - 1] = value;
                _values = null;
            }
        }
        public int Size { get => size; }
        private List<T> _values;
        public List<T> GetValues() {
            if (_values != null) return _values;
            _values = new List<T>(size * (size - 1) / 2);
            foreach (var list in matrix) {
                _values.AddRange(list);
            }
            return _values;
        }
        #endregion

        #region Private fields
        private List<List<T>> matrix;
        private int size;
        private T _default;
        #endregion
    }
}
