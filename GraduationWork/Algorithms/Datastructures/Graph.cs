﻿using System;
using System.Collections.Generic;

namespace Algorithms.Datastructures {
    public class Graph {
        #region Constructors
        public Graph() {
            incidencyLists = null;
            adjacencyMatrix = null;
            currentRepresentation = GraphRepresentation.None;
        }
        public Graph(IncidencyLists incidencyLists) {
            AsIncidencyLists = incidencyLists;
            currentRepresentation = GraphRepresentation.IncidencyLists;
            _order = null;
            _size = null;
        }
        public Graph(AdjacencyMatrix incidencyMatrix) {
            AsIncidencyMatrix = incidencyMatrix;
            currentRepresentation = GraphRepresentation.AdjacencyMatrix;
            _order = null;
            _size = null;
        }
        #endregion Constructors

        #region Transformations
        public IncidencyLists AsIncidencyLists {
            set {
                incidencyLists = value as IncidencyLists;
                if (incidencyLists == null)
                    throw new ArgumentNullException("Can't assign to null");
                adjacencyMatrix = null;
                _order = null;
                _size = null;
                currentRepresentation = GraphRepresentation.IncidencyLists;
            }
            get {
                // if lists already exist, return them
                if (incidencyLists != null)
                    return incidencyLists;
                // if not, create them
                List<List<int>> lists = new List<List<int>>();
                for (int i = 0; i < adjacencyMatrix.Size; ++i) {
                    List<int> incidency = new List<int>();
                    for (int j = 0; j < adjacencyMatrix.Size; ++j) {
                        if (adjacencyMatrix[i, j])
                            incidency.Add(j);
                    }
                    lists.Add(incidency);
                }
                incidencyLists = new IncidencyLists(lists);
                // return newly created lists
                return incidencyLists;
            }
        }
        public AdjacencyMatrix AsIncidencyMatrix {
            set {
                adjacencyMatrix = value as AdjacencyMatrix;
                if (adjacencyMatrix == null)
                    throw new ArgumentNullException("Can't assign to null");
                incidencyLists = null;
                _order = null;
                _size = null;
                currentRepresentation = GraphRepresentation.AdjacencyMatrix;
            }
            get {
                // if matrix has already been created, return it
                if (adjacencyMatrix != null)
                    return adjacencyMatrix;
                // if matrix hasn't been created, create it
                int size = incidencyLists.Count;
                bool[,] matrix = new bool[size, size];
                for (int i = 0; i < size; ++i) {
                    for (int j = 0; j < size; ++j) {
                        matrix[i, j] = incidencyLists[i].Contains(j);
                    }
                }
                adjacencyMatrix = new AdjacencyMatrix(matrix, size);
                // return newly created matrix
                return adjacencyMatrix;
            }
        }
        #endregion Transformations

        #region Public properties
        #region Order
        private int? _order;
        /// <summary> Number of verticies in graph.</summary>
        public int Order {
            get {
                if (_order == null) {
                    CalculateOrder(currentRepresentation);
                }
                return (int)_order;
            }
        }
        /// <summary>
        /// Calculates Order of this graph by given type of representation.
        /// </summary>
        /// <param name="graphRepresentation">By wich type of representation the Order must be calculated</param>
        private void CalculateOrder(GraphRepresentation graphRepresentation) {
            switch (graphRepresentation) {
            case GraphRepresentation.IncidencyLists: {
                if (incidencyLists == null)
                    throw new ArgumentException("can't calculate order for uninitialized param", nameof(graphRepresentation));
                _order = incidencyLists.Count;
            }
            break;
            case GraphRepresentation.AdjacencyMatrix: {
                if (adjacencyMatrix == null)
                    throw new ArgumentException("can't calculate order for uninitialized param", nameof(graphRepresentation));
                _order = adjacencyMatrix.Size;
            }
            break;
            default: {
                throw new ArgumentException("can't calculate order for given param", nameof(graphRepresentation));
            }
            }
        }
        #endregion
        #region Size
        /// <summary>hidden value for Size</summary>
        private int? _size;
        /// <summary>Number of edges in graph.</summary>
        public int Size {
            get {
                if (_size == null) {
                    CalculateSize(currentRepresentation);
                }
                return (int)_size;
            }
        }
        /// <summary>
        /// Calculates Order of this graph by given type of representation.
        /// </summary>
        /// <param name="graphRepresentation">By wich type of representation the Order must be calculated</param>
        private void CalculateSize(GraphRepresentation graphRepresentation) {
            switch (graphRepresentation) {
            case GraphRepresentation.IncidencyLists: {
                if (incidencyLists == null)
                    throw new ArgumentException("can't calculate size for uninitialized param", nameof(graphRepresentation));
                int count = 0;
                for (int i = 0; i < incidencyLists.Count; ++i) {
                    count += incidencyLists[i].Count;
                }
                count /= 2;
                _size = count;
            }
            break;
            case GraphRepresentation.AdjacencyMatrix: {
                if (adjacencyMatrix == null)
                    throw new ArgumentException("can't calculate order for uninitialized param", nameof(graphRepresentation));
                int count = 0;
                for (int i = 0; i < adjacencyMatrix.Size; ++i) {
                    for (int j = 0; j < adjacencyMatrix.Size; ++j) {
                        if (adjacencyMatrix[i, j])
                            count += 1;
                    }
                }
                count /= 2;
                _size = count;
            }
            break;
            default: {
                throw new ArgumentException("can't calculate order for given param", nameof(graphRepresentation));
            }
            }
        }
        #endregion
        /// <summary>
        /// Calculates degrees of vertices of the graph
        /// </summary>
        /// <returns>array d[], where d[i] is a degree of vertex i</returns>
        public int[] GetDegrees() {
            int[] degrees = new int[Order];
            switch (currentRepresentation) {
            case GraphRepresentation.IncidencyLists: {
                for (int vertex = 0; vertex < Order; ++vertex) {
                    degrees[vertex] = incidencyLists[vertex].Count;
                }
            }
            break;
            case GraphRepresentation.AdjacencyMatrix: {
                for (int vertex = 0; vertex < Order; ++vertex) {
                    int degree = 0;
                    for (int i = 0; i < Order; ++i) {
                        if (adjacencyMatrix[vertex, i])
                            ++degree;
                    }
                    degrees[vertex] = degree;
                }
            }
            break;
            }
            return degrees;
        }
        #endregion

        #region Private fields and members
        private enum GraphRepresentation { None, IncidencyLists, AdjacencyMatrix, EdgeList }
        private GraphRepresentation currentRepresentation;
        private IncidencyLists incidencyLists;
        private AdjacencyMatrix adjacencyMatrix;
        #endregion
    }
}
