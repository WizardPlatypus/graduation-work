﻿using System;
using System.Collections.Generic;

namespace Algorithms.Datastructures {
    public class WeightedMatrix : FlatMatrix<int> {
        #region Constructors
        public WeightedMatrix(int size, int[,] source)
            : base(source, size) { }
        public WeightedMatrix(int size)
            : base(size) { }
        #endregion

        #region Transformations
        public AdjacencyMatrix AsMatrix {
            get {
                AdjacencyMatrix matrix = new AdjacencyMatrix(Size);
                for (int i = 0; i < Size; ++i) {
                    for (int j = 0; j < Size; ++j) {
                        bool areAdjacent = true;
                        if (this[i, j] == 0)
                            areAdjacent = false;
                        matrix[i, j] = matrix[j, i] = areAdjacent;
                    }
                }
                return matrix;
            }
        }
        #endregion

        /// <summary>
        /// Generates random weighted matrix with secified parameters
        /// </summary>
        /// <param name="size">Size of desired matrix</param>
        /// <param name="max_weight">max weight of edges in desired matrix</param>
        /// <param name="min_degree">min degree of vertices in desired matrix</param>
        /// <returns>Generated matrix with given parameters</returns>
        public static WeightedMatrix GetRandom(int size, int max_weight = 15, int min_degree = 2) {
            var gen = new Random();
            var matrix = new WeightedMatrix(size);
            for (int i = 0; i < size; ++i) {
                for (int j = i + 1; j < size; ++j) {
                    matrix[i, j] = gen.Next(1, max_weight + 1);
                }
            }
            for (int i = 0; i < size; ++i) {
                //previously generated matrix is for complete graph
                int spare_degree = size - 1 - i - min_degree;
                if (spare_degree <= 0)
                    continue;
                //how many times we will try to kill a random edge
                int count = gen.Next(0, spare_degree + 1);
                while (count-- != 0) {
                    // random edge gets killed
                    // it might be already dead tho
                    int j = gen.Next(i + 1, size);
                    matrix[i, j] = 0;
                }
            }
            return matrix;
        }
        /// <summary>
        /// Generates random weight matrix for graph with given order, size and minimal degree of each vertex.
        /// </summary>
        /// <param name="order">number of vertices</param>
        /// <param name="size">number of edges</param>
        /// <param name="max_weight">maximal weight of an edge</param>
        /// <param name="min_degree">minimal degree of each vertex</param>
        /// <returns></returns>
        public static WeightedMatrix GetRandom(in int order, in int size, int max_weight = 15, int min_degree = 2) {
            if (size < MinSizeForDegreeOnOrder(order, min_degree))
                throw new ArgumentException("Can't generate graph of such size with given order and minimal degree: not enough edges");
            var gen = new Random();
            var matrix = new WeightedMatrix(order);
            for (int i = 0; i < order; i++) {
                for (int j = i + 1; j < order; j++) {
                    matrix[i, j] = gen.Next(1, max_weight + 1);
                }
            }
            int edge_count = order * (order - 1) / 2;
            HashSet<(int, int)> locked_edges = new HashSet<(int, int)>(edge_count);
            int[] degree = new int[order];
            Array.Fill(degree, order - 1);
            // we will try to delete edges untill we reach the desired
            // size of the graph
            while (edge_count > size) {
                int victim_index = gen.Next(0, edge_count - locked_edges.Count);
                int current_edge = 0;
                for (int i = 0; i < order; i++) {
                    for (int j = i + 1; j < order; j++) {
                        if (degree[j] <= min_degree || degree[i] <= min_degree || matrix[i, j] == 0)
                            // can't delete edges adjacent to the
                            // vertices with degree equal or less
                            // than minimal
                            continue;
                        if (current_edge == victim_index) {
                            // if we have counted "victim" deletable
                            // edges, remove current one
                            if (matrix[i, j] == 0)
                                throw new Exception("Some edges are getting deleted twice.");
                            matrix[i, j] = 0;
                            degree[i]--;
                            degree[j]--;
                            if (degree[i] <= min_degree || degree[j] <= min_degree)
                                locked_edges.Add((i, j));
                            edge_count--;
                            goto NEXT_VICTIM;
                        }
                        // counting deletable edges
                        current_edge++;
                    }
                }
                throw new Exception("This place in code should not be ever reachable");
            NEXT_VICTIM:;
            }
            if (edge_count > size)
                throw new Exception("Sth wrong with the while loop above");
            return matrix;
        }
        /// <summary>
        /// Calculates minimal number of edges in a graph of given order with given minimal degree of it's vertices
        /// </summary>
        /// <param name="order">Order of graph</param>
        /// <param name="degree">Minimal degree of vertices of the graph</param>
        /// <returns>Minimal number of edges in a graph of given order with given minimal degree of it's vertices</returns>
        private static int MinSizeForDegreeOnOrder(int order, int degree) {
            return (degree + 1) * degree / 2 + (order - degree - 1) * degree;
        }

        public override string ToString() {
            string res = "";
            for (int i = 0; i < Size; ++i) {
                res += "{ ";
                for (int j = 0; j < Size; ++j) {
                    res += String.Format("{0,2}", this[i, j]);
                    if (j + 1 != Size) {
                        res += ", ";
                    } else {
                        res += " ";
                    }
                }
                res += "}\n";
            }
            return res;
        }
    }
}
