﻿using System;
using System.Collections.Generic;

namespace Algorithms.Datastructures {
    public class IncidencyLists {
        private List<List<int>> _incidencyList;
        public IncidencyLists(List<List<int>> incidencyList = null) {
            _incidencyList = incidencyList;
        }
        public List<int> this[int index] {
            get {
                if (index < 0 || index >= _incidencyList.Count)
                    throw new IndexOutOfRangeException();
                return _incidencyList[index];
            }
        }
        public int Count {
            get {
                return _incidencyList.Count;
            }
        }
    }
}
