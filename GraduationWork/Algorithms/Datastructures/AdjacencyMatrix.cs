﻿namespace Algorithms.Datastructures {
    public class AdjacencyMatrix : FlatMatrix<bool> {
        public AdjacencyMatrix(int size = 0)
            : base(size) { }
        public AdjacencyMatrix(bool[,] source, int size)
            : base(source, size) { }
        public AdjacencyMatrix(int size, bool[,] source)
            : base(source, size) { }
        public AdjacencyMatrix(bool[,] source)
            : base(source, source.GetLength(0)) { }
    }
}
