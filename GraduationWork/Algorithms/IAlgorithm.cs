﻿namespace Algorithms {
    /// <summary>
    /// Base interface for all algorithms.
    /// </summary>
    public interface IAlgorithm {
        #region Behavior
        /// <summary>
        /// Starts an execution of the algorithm
        /// </summary>
        public void Start();
        /// <summary>
        /// Sets input for the algorithm
        /// </summary>
        /// <param name="input">Input to be set for the algorithm</param>
        public void SetInput(Input input);
        /// <summary>
        /// Gives back result of work of the algorithm
        /// </summary>
        /// <returns>Result of the algorithm</returns>
        public Result GetResult();
        #endregion

        #region Public member classes
        /// <summary>
        /// Base class for all inputs.
        /// Different algorithms might require different inputs,
        /// although polymorphism cannot be used on functions with
        /// different number & types of parameters, so empty base
        /// classes are used instead. This way, every algorithm
        /// implements an Input class with properties which would
        /// fulfill algorithm's needs.
        /// </summary>
        public abstract class Input { };
        /// <summary>
        /// Base class for all results.
        /// Different algorithm might produce different results with
        /// different number of properties of different types, so
        /// a custom result-containing class is needed for each
        /// algorithm, and in order to use polymorphism all of the
        /// result classes must derive from one empty class.
        /// </summary>
        public abstract class Result { };
        #endregion
    }
    public class Perform<T> where T : IAlgorithm, new() {
        public static IAlgorithm.Result On(IAlgorithm.Input input) {
            var algorithm = new T();
            algorithm.SetInput(input);
            algorithm.Start();
            return algorithm.GetResult();
        }
    }
}
