﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Datastructures;

namespace Algorithms {
    /// <summary>
    /// Algorithm to find all cycles in graph
    /// </summary>
    public class FindAllCyclesGraph : IAlgorithm {
        public void Start() {
            cycles = new List<List<int>>(); // final list of cycles
            // to keep track of used edges
            used = new FlatMatrix<bool>(graph.Order);
            // /*
            // all of the cycles are going to be outgrown copies of this one
            cycle = new Stack<int>(graph.Order + 3);
            cycle.Push(origin);
            // asuming that cycle might contain Order + 1(for target) + 2(for repetitions) vertices
            foreach (var incident in graph.AsIncidencyLists[origin]) {
                // to prevent reversed copies of cycles
                origin_edge = incident;
                // Add incident to cycle
                cycle.Push(incident);
                used[origin, incident] = true;
                // Search
                Search();
                // Remove incident from cycle
                cycle.Pop();
                used[origin, incident] = false;
            }
            return; // */
            /*
            int[] stack = new int[graph.Order * 2];
            int count = 0;
            stack[count++] = origin;
            // int[] next_incident = new int[graph.Order];
            while (count > 0)
            {
                int current = stack[count-1];
                int incident = -1;
                /*
                for(int i = next_incident[current];
                    i < graph.AsIncidencyLists[current].Count;
                    i++)
                {
                    if (used[graph.AsIncidencyLists[current][i], current])
                        continue;
                    incident = graph.AsIncidencyLists[current][i];
                    next_incident[current] = i + 1;
                    break;
                }
                foreach(var v in graph.AsIncidencyLists[current])
                {
                    if (used[v, current]) continue;
                    incident = v;
                    break;
                }
                if (incident == -1)
                {
                    count--;
                    if (count == 0) break;
                    used[stack[count-1], current] = false;
                    // next_incident[current] = 0;
                    continue;
                }
                if (incident == origin)
                {
                    if (current > stack[1])
                    {
                        stack[count++] = origin;
                        cycles.Add(stack.Take(count).ToList());
                        count--;
                    }
                    continue;
                }
                stack[count++] = incident;
                used[current, incident] = true;
            }
            // */
        }

        #region Algorithm
        void Search() {
            var last = cycle.Peek();
            foreach (var incident in graph.AsIncidencyLists[last]) {
                // can't use same edge twice
                if (used[last, incident])
                    continue;
                if (incident == origin) {
                    // to prevent reversed copies of the same cycle
                    if (last > origin_edge) {
                        cycle.Push(origin);
                        cycles.Add(cycle.ToList());
                        cycle.Pop();
                    }
                    continue;
                }
                // Add incident to cycle
                cycle.Push(incident);
                used[last, incident] = true;
                // Search
                Search();
                // Remove incident from cycle
                cycle.Pop();
                used[last, incident] = false;
            }
        }
        #endregion

        #region Public member classes
        public class Input : IAlgorithm.Input {
            public Graph Graph;
            public int TargetVertex { get; set; }
            public Input(Graph graph, int target_vertex) {
                Graph = graph;
                TargetVertex = target_vertex;
            }
        }
        public class Result : IAlgorithm.Result {
            public List<List<int>> Cycles;
        }
        #endregion

        #region Input/Result
        public IAlgorithm.Result GetResult() {
            return new Result() { Cycles = cycles };
        }
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as Input;
            graph = _input.Graph;
            origin = _input.TargetVertex;
        }
        #endregion Input/Result

        #region Private fields
        private Graph graph;
        private FlatMatrix<bool> used;
        private int origin_edge;
        private int origin;
        private Stack<int> cycle;
        private List<List<int>> cycles;
        #endregion
    }
}
