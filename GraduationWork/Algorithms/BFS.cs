using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms.Datastructures;

namespace Algorithms {
    /// <summary>
    /// Breadth First Search algorithm
    /// </summary>
    public class BFS : IAlgorithm {
        public virtual void Start() {
            sequence = new List<int>();
            visited = new bool[graph.Order];
            distances = new int[graph.Order];
            Search(startVertex);
        }

        #region Algorithm
        protected virtual void Search(int current = 0) {
            Queue<int> queue = new Queue<int>();
            sequence.Add(current);
            queue.Enqueue(current);
            visited[current] = true;
            while (queue.Count > 0) {
                var vertex = queue.Dequeue();
                foreach (var victim in graph.AsIncidencyLists[vertex]) {
                    if (!visited[victim]) {
                        sequence.Add(victim);
                        queue.Enqueue(victim);
                        visited[victim] = true;
                        distances[victim] = distances[vertex] + 1;
                    }
                }
            }
        }
        #endregion

        #region Input/Result
        public void SetInput(IAlgorithm.Input input) {
            var _input = input as BFS.Input;
            if (input == null)
                throw new ArgumentNullException();
            graph = _input.Graph;
            startVertex = _input.StartVertex;
        }
        public IAlgorithm.Result GetResult() {
            return new BFS.Result(distances.Max(), sequence.ToArray(), distances);
        }
        #endregion

        #region Public member classes
        public class Input : IAlgorithm.Input {
            private Graph graph;
            public int StartVertex { get; set; }
            public Input(Graph graph, int startVertex) {
                this.graph = graph;
                StartVertex = startVertex;
            }
            public Graph Graph { get => graph; set => graph = value; }
        }
        public class Result : IAlgorithm.Result {
            public int MaxDepth { set; get; }
            public IEnumerable<int> Distances { set; get; }
            public IEnumerable<int> Sequence { set; get; }
            public Result(int maxDepth, int[] sequence, int[] distances) {
                MaxDepth = maxDepth;
                Sequence = sequence;
                Distances = distances;
            }
        }
        #endregion

        #region Private fields
        // These fields are marked as protected in order to allow
        // deriving classes to inherit them
        protected Graph graph;
        protected bool[] visited;
        protected List<int> sequence;
        protected int[] distances;
        protected int startVertex;
        #endregion
    }
    public class RecBFSbyIncidencyMatrix : BFS {
        // this implementation was inspired by https://www.techiedelight.com/breadth-first-search/
        protected Queue<int> queue;
        public override void Start() {
            sequence = new List<int>();
            visited = new bool[graph.Order];
            distances = new int[graph.Order];
            queue = new Queue<int>();
            queue.Enqueue(startVertex);
            sequence.Add(startVertex);
            visited[startVertex] = true;
            Search();
        }
        protected override void Search(int current = 0) {
            if (queue.Count == 0)
                return;
            var vertex = queue.Dequeue();
            for (int victim = 0; victim < graph.Order; ++victim) {
                if (graph.AsIncidencyMatrix[vertex, victim] && !visited[victim]) {
                    visited[victim] = true;
                    queue.Enqueue(victim);
                    sequence.Add(victim);
                    distances[victim] = distances[vertex] + 1;
                }
            }
            Search();
        }
    }
    public class RecBFSbyIncidencyLists : BFS {
        // this implementation was inspired by https://www.techiedelight.com/breadth-first-search/
        protected Queue<int> queue;
        public override void Start() {
            sequence = new List<int>();
            visited = new bool[graph.Order];
            distances = new int[graph.Order];
            queue = new Queue<int>();
            queue.Enqueue(startVertex);
            sequence.Add(startVertex);
            visited[startVertex] = true;
            Search();
        }
        protected override void Search(int current = 0) {
            if (queue.Count == 0)
                return;
            var vertex = queue.Dequeue();
            foreach (var victim in graph.AsIncidencyLists[vertex]) {
                if (!visited[victim]) {
                    visited[victim] = true;
                    queue.Enqueue(victim);
                    sequence.Add(victim);
                    distances[victim] = distances[vertex] + 1;
                }
            }
            Search();
        }

    }
    public class NonRecBFSbyIncidencyMatrix : BFS {
        // this implementation was inspired by https://www.techiedelight.com/breadth-first-search/
        protected override void Search(int current = 0) {
            Queue<int> queue = new Queue<int>();
            sequence.Add(current);
            queue.Enqueue(current);
            visited[current] = true;
            while (queue.Count > 0) {
                var vertex = queue.Dequeue();
                for (int victim = 0; victim < graph.Order; ++victim) {
                    if (!graph.AsIncidencyMatrix[vertex, victim] || visited[victim]) {
                        continue;
                    }
                    sequence.Add(victim);
                    queue.Enqueue(victim);
                    visited[victim] = true;
                    distances[victim] = distances[vertex] + 1;
                }
            }
        }

    }
    public class NonRecBFSbyIncidencyLists : BFS { /* whole implementation comes from base */ }
}
