﻿using System.Collections.Generic;
using System.Drawing;
using Xunit;

namespace UserInterface.Tests {
    public class DotTests {
        static Dot _D(int x, int y) {
            return new Dot(new Point(x, y));
        }
        private class DotPointContainmentCollection : IEnumerable<object[]> {
            public IEnumerator<object[]> GetEnumerator() {
                yield return new object[]
                {
                    _D(0, 0), // element
                    new Point(0, 1), // point
                    true // containment
                };
                yield return new object[]
                {
                    _D(0,0), // element
                    new Point(0, 5), // point
                    true // containment
                };
                yield return new object[]
                {
                    _D(0,0), // element
                    new Point(2, 3), // point
                    true // containment
                };
                yield return new object[]
                {
                    _D(16, 12), // element
                    new Point(9, 6), // point
                    true // containment
                };
                yield return new object[]
                {
                    _D(16, 12), // element
                    new Point(14, 13), // point
                    true // containment
                };
                yield return new object[]
                {
                    _D(16, 12), // element
                    new Point(18, 15), // point
                    true // containment
                };
            }
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
                return GetEnumerator();
            }
        }

        private class DotPointTouched : IEnumerable<object[]> {
            public IEnumerator<object[]> GetEnumerator() {
                yield return new object[]
                {
                    _D(0, 0), // dot
                    new Point(0, 1), // point
                    true // touched
                };
                yield return new object[]
                {
                    _D(0,0),
                    new Point(0, 5),
                    true
                };
                yield return new object[]
                {
                    _D(0,0),
                    new Point(2, 3),
                    true
                };
                yield return new object[]
                {
                    _D(16, 12),
                    new Point(9, 6),
                    true
                };
                yield return new object[]
                {
                    _D(16, 12),
                    new Point(14, 13),
                    true
                };
                yield return new object[]
                {
                    _D(16, 12),
                    new Point(18, 15),
                    true
                };
            }
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
                return GetEnumerator();
            }
        }

        [Theory]
        [ClassData(typeof(DotPointContainmentCollection))]
        public void ShouldContainGivenPoint(Dot dot, Point givenPoint, bool expected) {
            // Given: element, point
            // When:
            bool actual = dot.Contains(givenPoint);
            // Then:
            Assert.Equal(expected, actual);
        }
        [Theory]

        [ClassData(typeof(DotPointTouched))]
        public void ShouldBeTouchedByGivenPoint(Dot dot, Point givenPoint, bool expected) {
            // Given: element, point
            // When:
            bool actual = dot.TouchedBy(givenPoint);
            // Then:
            Assert.Equal(expected, actual);
        }
    }
}
