﻿using System.Collections.Generic;
using System.Drawing;
using Xunit;

namespace UserInterface.Tests {
    public class LineTests {
        private class LinePointContainmentCollection : IEnumerable<object[]> {
            private Dot _D(int x, int y) {
                return new Dot(new Point(x, y));
            }
            private Line _L(int x1, int y1, int x2, int y2) {
                return new Line(_D(x1, y1), _D(x2, y2));
            }
            private Point _P(int x, int y) {
                return new Point(x, y);
            }
            public IEnumerator<object[]> GetEnumerator() {
                yield return new object[]
                {
                    _L(-1, 0, 1, 6), // line
                    _P(2, 3), // point
                    true // touched
                };
                yield return new object[]
                {
                    _L(-1, 0, 2, 2),
                    _P(2, 3),
                    false
                };
                yield return new object[]
                {
                    _L(6, 0, 2, 2),
                    _P(2, 3),
                    false
                };
                yield return new object[]
                {
                    _L(8, 2, 2, 2),
                    _P(2, 3),
                    true
                };
                yield return new object[]
                {
                    _L(-4, 0, 6, 3),
                    _P(2, 3),
                    true
                };
                yield return new object[]
                {
                    _L(-4, 0, 6, 3),
                    _P(1, 5),
                    false
                };
                yield return new object[]
                {
                    _L(-5, 9, 0, -3),
                    _P(1, 5),
                    false
                };
                yield return new object[]
                {
                    _L(-3, 8, 0, 1),
                    _P(1, 5),
                    true
                };
                yield return new object[]
                {
                    _L(-5, 4, 5, 4),
                    _P(1, 5),
                    true
                };
                yield return new object[]
                {
                    _L(0, -2, 2, 2),
                    _P(3, 2),
                    false
                };
                yield return new object[]
                {
                    _L(0, -2, 0, 2),
                    _P(3, 0),
                    true
                };
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
                return GetEnumerator();
            }
        }

        [Theory]
        [ClassData(typeof(LinePointContainmentCollection))]
        public void ShallBeTouchedByGivenPoint(Line line, Point givenPoint, bool expected) {
            // Given: element, point
            // When:
            bool actual = line.TouchedBy(givenPoint);
            // Then:
            Assert.Equal(expected, actual);
        }
    }
}
