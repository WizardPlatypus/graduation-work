﻿using System;
using System.Data;
using System.IO;
using Algorithms;
using Algorithms.Datastructures;

namespace ConsoleApp {
    //struct Record {
    //    /// Graph properties
    //    public int Order { get; set; }
    //    public int Size { get; set; }
    //    public int NumberOfCycles { get; set; }
    //    /// Time
    //    // time traveled by singular salesman alone
    //    // distance traveled by singular salesman alone
    //    public int SingleTime { get; set; }
    //    // time traveled together by two salesmen
    //    public int MergedTime { get; set; }
    //    // distance traveled together by two salesmen
    //    public int MergedDistance { get; set; }
    //}
    class Program {
        static void Main(string[] args) {
            table = new DataTable();
            table.TableName = "TwoSalesmen-VS-SingleSalesman_Data";
            table.Columns.Add("Order");
            table.Columns.Add("Size");
            table.Columns.Add("NumberOfCycles");
            table.Columns.Add("SingleTime");
            table.Columns.Add("MergedTime");
            table.Columns.Add("MergedDistance");
            GenerateData();
            var dataWriter = new StreamWriter("data");
            table.WriteXml(dataWriter);
            dataWriter.Close();
        }
        static private void GenerateData(int min_order = 4, int max_order = 8) {
            int samples_per_order = 10;
            for (int order = min_order; order < max_order; order++) {
                for (int samples_of_order = 0; samples_of_order < samples_per_order; samples_of_order++) {
                    int size = Math.Max(order * 2 + 1, (int)((double)samples_of_order / samples_per_order * order * (order - 1) / 2));
                    Console.WriteLine($"Sample #{samples_of_order + 1} of order {order}: size = {size}");
                    var matrix = WeightedMatrix.GetRandom(order, size, 15, 2);
                    var algorithm = new TwoSalesmen();
                    algorithm.SetInput(new TwoSalesmen.Input(matrix, 0));
                    algorithm.Start();
                    var result = algorithm.GetResult() as TwoSalesmen.Result;
                    table.Rows.Add(
                        order,
                        size,
                        result.AllCycles.Count,
                        result.SingleWeight,
                        result.TotalWeight,
                        result.Weight1 + result.Weight2
                    );
                }
            }
        }
        static DataTable table;
    }
}
