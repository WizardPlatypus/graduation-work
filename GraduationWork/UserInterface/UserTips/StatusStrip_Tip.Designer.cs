﻿
namespace UserInterface.UserTips
{
    partial class StatusStrip_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StatusStrip_Tip));
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(360, 119);
            this.messageLabel.Text = resources.GetString("messageLabel.Text");
            // 
            // StatusStrip_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "StatusStrip_Tip";
            this.Size = new System.Drawing.Size(360, 138);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
