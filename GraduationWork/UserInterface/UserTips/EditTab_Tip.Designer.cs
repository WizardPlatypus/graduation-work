﻿
namespace UserInterface.UserTips
{
    partial class EditTab_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(383, 149);
            this.messageLabel.Text = "Для того щоб змінити вагу певного ребра, необхідно виділити це ребро, перейти до " +
    "вкладки «Зміни» в області вкладок, ввести ціле додатнє значення у текстове поле," +
    " та нажати кнопку «Застосувати зміни».";
            // 
            // EditTab_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "EditTab_Tip";
            this.Size = new System.Drawing.Size(383, 168);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
