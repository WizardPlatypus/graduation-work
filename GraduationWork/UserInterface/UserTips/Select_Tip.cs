﻿using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class Select_Tip : UserTip {

        public Select_Tip(InteractiveToolTip parent)
                : base(parent) {
            InitializeComponent();
        }
    }
}
