﻿
namespace UserInterface.UserTips
{
    partial class Draw_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.messageLabel.Size = new System.Drawing.Size(435, 124);
            this.messageLabel.Text = "Кнопка «Малювання» у меню надає доступ до кнопок «Ребро» та «Вершина», за допомог" +
    "ою яких можна змінити режим користування відповідно до «Малювання ребер» та «Мал" +
    "ювання вершин».";
            // 
            // Draw_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Margin = new System.Windows.Forms.Padding(7, 5, 7, 5);
            this.Name = "Draw_Tip";
            this.Size = new System.Drawing.Size(435, 143);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
