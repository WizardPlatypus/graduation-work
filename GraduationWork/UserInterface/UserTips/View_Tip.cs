﻿using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class View_Tip : UserTip {
        public View_Tip(InteractiveToolTip parent)
            : base(parent) {
            InitializeComponent();
        }
    }
}
