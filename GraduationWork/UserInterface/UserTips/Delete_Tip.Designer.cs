﻿
namespace UserInterface.UserTips
{
    partial class Delete_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(434, 69);
            this.messageLabel.Text = "Кнопка «Видалення» у меню надає доступ до кнопок «Видалити виділене» та «Видалити" +
    " усе», які відповідно видаляють виділений елемент моделі, або ж усю модель.";
            // 
            // Delete_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Delete_Tip";
            this.Size = new System.Drawing.Size(434, 88);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
