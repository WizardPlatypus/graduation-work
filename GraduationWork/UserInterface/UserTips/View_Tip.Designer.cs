﻿
namespace UserInterface.UserTips
{
    partial class View_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(390, 116);
            this.messageLabel.Text = "Кнопка «Модель» у меню надає доступ до кнопок «Зберегти» та «Відкрити», за допомо" +
    "гою яких можна відповідно зберегти намальовану модель у файл або ж відкрити вже " +
    "збережений файл з моделлю.";
            // 
            // View_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.Name = "View_Tip";
            this.Size = new System.Drawing.Size(390, 139);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
