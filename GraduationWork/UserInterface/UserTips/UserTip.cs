﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class UserTip : UserControl {
        // this constructor is needed for the designer; can't remove it, but it should not be used.
        public UserTip() {
            InitializeComponent();
        }
        public UserTip(InteractiveToolTip parent) {
            InitializeComponent();
            BackColor = Color.Transparent;
            this.parent = parent;
            Click += OnClick;
            exitLabel.Click += OnClick;
            messageLabel.Click += OnClick;
        }

        protected void OnClick(object sender, EventArgs args) {
            parent.Hide();
        }

        private InteractiveToolTip parent;
    }
}
