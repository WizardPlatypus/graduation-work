﻿using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class Draw_Tip : UserTip {
        public Draw_Tip(InteractiveToolTip parent)
            : base(parent) {
            InitializeComponent();
        }
    }
}
