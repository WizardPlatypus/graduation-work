﻿
namespace UserInterface.UserTips
{
    partial class TabControl_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(365, 72);
            this.messageLabel.Text = "Область вкладок розташована у правій частині вікна й вміщає у себе необхідні елем" +
    "енти для роботи з алгоритмом.";
            // 
            // TabControl_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "TabControl_Tip";
            this.Size = new System.Drawing.Size(365, 91);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
