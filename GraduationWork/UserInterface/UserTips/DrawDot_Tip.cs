﻿using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class DrawDot_Tip : UserTip {
        public DrawDot_Tip(InteractiveToolTip parent)
                : base(parent) {
            InitializeComponent();
        }
    }
}
