﻿using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class Move_Tip : UserTip {
        public Move_Tip(InteractiveToolTip parent)
                : base(parent) {
            InitializeComponent();
        }
    }
}
