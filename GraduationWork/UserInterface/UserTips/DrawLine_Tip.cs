﻿using Digitalis.GUI.Controls;

namespace UserInterface.UserTips {
    public partial class DrawLine_Tip : UserTip {
        public DrawLine_Tip(InteractiveToolTip parent)
                : base(parent) {
            InitializeComponent();
        }
    }
}
