﻿
namespace UserInterface.UserTips
{
    partial class Navigate_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(397, 94);
            this.messageLabel.Text = "Кнопка «Навігація» у меню надає доступ до кнопок «До Титульної Сторінки» та «Вихі" +
    "д», по яким відповідно можна повернутися до титульної сторінки або вийти з прогр" +
    "ами взагалі.";
            // 
            // Navigate_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Navigate_Tip";
            this.Size = new System.Drawing.Size(397, 113);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
