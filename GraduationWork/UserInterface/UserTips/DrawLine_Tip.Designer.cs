﻿
namespace UserInterface.UserTips
{
    partial class DrawLine_Tip
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // messageLabel
            // 
            this.messageLabel.Size = new System.Drawing.Size(360, 96);
            this.messageLabel.Text = "«Малювання ребер»: дозволяє з’єднувати вершини ребрами. Між кожною парою вершин м" +
    "оже існувати тільки одне ребро. У рядку стану цей режим позначається як «DrawLin" +
    "e».";
            // 
            // DrawLine_Tip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DrawLine_Tip";
            this.Size = new System.Drawing.Size(360, 115);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
