﻿using System.Drawing;

namespace UserInterface {
    public abstract class ViewElement {
        #region Drawing
        /// <summary>
        /// Draws element on given Graphics
        /// </summary>
        /// <param name="g">Graphics to be drawn on</param>
        public abstract void Draw(Graphics g);
        /// <summary>
        /// Clears space obtained by element on given Graphics
        /// </summary>
        /// <param name="g">Graphics to be cleared from</param>
        public abstract void Clear(Graphics g);
        #endregion

        #region Attitudes
        /// <summary>
        /// Answers if given point is contained
        /// </summary>
        /// <param name="point">point to check containment for</param>
        /// <returns>true if given point is contained, false otherwise</returns>
        public abstract bool Contains(Point point);
        /// <summary>
        /// Answers if given point 'touches'('affects') element
        /// </summary>
        /// <param name="point">point to check touchness for</param>
        /// <returns>true if point 'touches' the element, false otherwise</returns>
        public abstract bool TouchedBy(Point point);
        /// <summary>
        /// Answers if element is inside given rectangle.
        /// </summary>
        /// <param name="rectangle">rectangle to check containment of</param>
        /// <returns></returns>
        public abstract bool isContainedBy(Rectangle rectangle);
        #endregion

        #region State flexibility
        /// <summary>
        /// Moves element to a new position
        /// </summary>
        /// <param name="position">new position</param>
        /// <returns>true if movement was succesfull, false otherwise</returns>
        public abstract bool MoveTo(Point position);
        public abstract void Highlight(Style style);
        /// <summary>
        /// Resets style of element
        /// </summary>
        public abstract void ResetStyle();
        #endregion

        #region Public properties
        public abstract bool Selected { get; set; }
        public abstract Style Style { get; set; }
        #endregion
    }
}
