﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UserInterface {
    public partial class Title : Form {
        public Title() {
            InitializeComponent();
        }

        #region Drag and move
        private Point _mouseLocation;
        private void this_MouseDown(object sender, MouseEventArgs e) {
            _mouseLocation = e.Location;
        }
        private void this_MouseMove(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                int dx = e.Location.X - _mouseLocation.X;
                int dy = e.Location.Y - _mouseLocation.Y;
                Location = new Point(this.Location.X + dx, this.Location.Y + dy);
            }
        }
        #endregion

        #region Ways to exit
        // this one doesn't work for some reason
        private void this_KeyDown(object sender, KeyEventArgs args) {
            if (args.KeyCode == Keys.Escape) Application.Exit();
        }
        private void exitButtonClick(object sender, EventArgs args) {
            Application.Exit();
        }
        #endregion

        #region Navigation
        public void NavigatorButtonClick(object sender, EventArgs args) {
            OnGoToNavigator();
        }
        public delegate void GoToNavigator();
        public event GoToNavigator OnGoToNavigator;
        #endregion
    }
}
