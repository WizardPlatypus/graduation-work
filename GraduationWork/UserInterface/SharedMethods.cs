﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace UserInterface {
    class SharedMethods {
        #region Math
        public static double Distance(double x1, double y1, double x2, double y2) {
            double dx, dy;
            dx = x2 - x1;
            dy = y2 - y1;
            return Math.Sqrt(dx * dx + dy * dy);
        }
        #endregion

        #region Drawing
        public static void DrawText(Graphics g, string text, Font font, Color color, Rectangle bounds) {
            TextRenderer.DrawText(g, text, font, bounds, color, TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter);
        }
        public static void DrawFluffyEllipse(Graphics g, int x, int y, int width, int height) {
            var path = new GraphicsPath();
            path.AddEllipse(x, y, width, height);
            PathGradientBrush brush = new PathGradientBrush(path);
            brush.CenterColor = Color.FromArgb(255, 255, 255, 255);
            Color[] colors = { Color.FromArgb(0, 0, 0, 0) };
            brush.SurroundColors = colors;
            g.FillEllipse(brush, x, y, width, height);
        }
        #endregion
    }
}
