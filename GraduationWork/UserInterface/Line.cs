﻿using System;
using System.Drawing;

namespace UserInterface {
    /// <summary>
    /// Class representing Line, aka Edge in graph
    /// </summary>
    public class Line : ViewElement {
        #region Constructors
        public Line(Dot start, Dot end) {
            this.start = start;
            this.end = end;
            Weight = 0;
            Style = Style.Default;
        }
        #endregion

        #region Drawing
        public override void Draw(Graphics g) {
            var style = Style;
            // drawing line
            Pen Peter = new Pen(style.StrokeColor, style.StrokeWidth) { DashStyle = style.DashStyle };
            g.DrawLine(Peter, start.Center, end.Center);
            // drawing text
            int r = 25;
            Rectangle bounds = new Rectangle() {
                X = (start.X + end.X) / 2 - r,
                Y = (start.Y + end.Y) / 2 - r,
                Width = r * 2,
                Height = r * 2
            };
            SharedMethods.DrawFluffyEllipse(g, bounds.X, bounds.Y, bounds.Width, bounds.Height);
            SharedMethods.DrawText(g, Weight.ToString(), style.TextFont, style.TextColor, bounds);
        }
        public override void ResetStyle() {
            _highlighted = false;
            if (_selected) {
                _style = Style.Selected;
            } else {
                _style = Style.Default;
            }
        }
        public override void Clear(Graphics g) {
            var style = Style.Blank;
            // drawing line
            float overlap = 2;
            Pen Peter = new Pen(style.StrokeColor, style.StrokeWidth + overlap);
            g.DrawLine(Peter, start.Center, end.Center);
            // drawing text
            int r = 25;
            Rectangle bounds = new Rectangle() {
                X = (start.X + end.X) / 2 - r,
                Y = (start.Y + end.Y) / 2 - r,
                Width = r * 2,
                Height = r * 2
            };
            g.FillEllipse(new SolidBrush(style.FillColor), bounds.X, bounds.Y, bounds.Width, bounds.Height);
            //SharedMethods.DrawFluffyEllipse(g, bounds.X, bounds.Y, bounds.Width, bounds.Height);
            //SharedMethods.DrawText(g, Weight.ToString(), style.TextFont, style.TextColor, bounds);

        }
        #endregion

        #region Containment
        public override bool TouchedBy(Point point) {
            lastAsked = point;
            double R = 3.16;
            // cos alfa = (a * b)/(|a| * |b|)
            // where a, b — vectors
            // if cos alfa >= 0, then alfa є [-90; 90];
            double a_x, a_y, b_x, b_y;
            double cos;
            a_x = start.X - end.X;
            a_y = start.Y - end.Y;
            b_x = point.X - end.X;
            b_y = point.Y - end.Y;
            cos = (a_x * b_x + a_y * b_y) / (SharedMethods.Distance(0, 0, a_x, a_y) * SharedMethods.Distance(0, 0, b_x, b_y));
            if (cos < 0)
                return false;
            // in case a is coliniar to b
            double my_epsilon = 0.5;
            if (Math.Abs(a_x / b_x - a_y / b_y) <= my_epsilon) {
                return point.X < Math.Max(start.X, end.X) && point.X > Math.Min(start.X, end.X) &&
                    point.Y < Math.Max(start.Y, end.Y) && point.Y > Math.Min(start.Y, end.Y);
            }
            // in case the line is vertical
            if (end.X == start.X) {
                // case when point is out of line's boundaries has already been checked
                return Math.Abs(point.Y - start.Y) <= R;
            }
            // if start.X == end.X then if point.x is in [start.X - R; start.X + R], do cross
            // y = kx + d : line equation
            // (x - a)^2 + (y - b)^2 = R^2 : circle equation, center is O(a, b), radius is R
            // x^2 + a^2 - 2ax + y^2 + b^2 - 2by = R^2 | opened brackets
            // x^2 + a^2 - 2ax + (kx + d)^2 + b^2 - 2b(kx + d) = R^2 | inserted expression of y from line equation
            // x^2 + a^2 - 2ax + k^2*x^2 + d^2 + 2kdx + b^2 - 2kbx - 2bd = R^2 | opened brackets
            // x^2*(1 + k^2) + x*(-2a + 2kd - 2kb) + (a^2 + b^2 + d^2 - R^2 - 2bd) = 0 | rearrenged to meet quadric equation form
            // A*x^2 + B*x + C = 0 | quadric equation
            // A = (1 + k^2), B = 2*(kd - kb - a), C= (a^2 + b^2 + d^2 - R^2 - 2bd) | labeling
            // D = B^2 - 4AC | Determiner formula
            // if D >= 0 then line y = kx + d and circle (x - a)^2 + (y - b)^2 = R^2 do cross
            double A, B, C, D, k, a, b, c, d;
            k = (double)(end.Y - start.Y) / (end.X - start.X);
            d = end.Y - k * end.X;
            a = point.X;
            b = point.Y;
            A = (1 + k * k);
            B = 2 * (k * d - k * b - a);
            C = (a * a + b * b + d * d - R * R - 2 * b * d);
            D = B * B - 4 * A * C;
            return D >= 0;
        }
        public override bool Contains(Point point) {
            int minX = Math.Min(start.X, end.X),
                maxX = Math.Max(start.X, end.X),
                minY = Math.Min(start.Y, end.Y),
                maxY = Math.Max(start.Y, end.Y);
            // quick check if point is in bounds of the line
            if (point.X < minX ||
                point.X > maxX ||
                point.Y < minY ||
                point.Y > maxY)
                return false; // if it isn't, we're done
                              // if it is, then check whether vectors
                              // <start, point> and <end, point> are collinear.
            /* A = start as Point, B = end as Point, C = point as Point
             * <CA> = <x1, y1> = <A.X - C.X, A.Y - C.Y>
             * <CB> = <x2, y2> = <B.X - C.X, B.Y - C.Y>
             * if <CA> and <CB> are collinear, then
             * x1 / x2 = y1 / y2, which implies 
             * x1 * y2 = x2 * y1
             * */
            int dx1 = start.X - point.X,
                dy1 = start.Y - point.Y,
                dx2 = end.X - point.X,
                dy2 = end.Y - point.Y;
            return dx1 * dy2 == dx2 * dy1;
        }
        public override bool isContainedBy(Rectangle rectangle) {
            int minX = Math.Min(start.X, end.X),
                maxX = Math.Max(start.X, end.X),
                minY = Math.Min(start.Y, end.Y),
                maxY = Math.Max(start.Y, end.Y);
            return rectangle.Contains(new Point(minX, minY)) &&
                rectangle.Contains(new Point(maxX, maxY));
        }
        #endregion

        #region State flexibility
        public override bool MoveTo(Point position) {
            int dx, dy;
            dx = position.X - lastAsked.X;
            dy = position.Y - lastAsked.Y;
            start.X += dx;
            start.Y += dy;
            end.X += dx;
            end.Y += dy;
            lastAsked = position;
            return true;
        }
        public void Randomize() {
            Weight = gen.Next(1, 20);
        }
        public override void Highlight(Style style) {
            if (!_highlighted) {
                Style = style;
                _highlighted = true;
                return;
            }
            Style += style;
        }
        #endregion

        #region Public properties
        public int X1 { get => start.X; set => start.X = value; }
        public int Y1 { get => start.Y; set => start.Y = value; }
        public int X2 { get => end.X; set => end.X = value; }
        public int Y2 { get => end.Y; set => end.Y = value; }
        public Dot Start { get => start; set => start = value; }
        public Dot End { get => end; set => end = value; }
        public int Weight { get; set; }
        public override bool Selected {
            get => _selected;
            set {
                _selected = value;
                ResetStyle();
            }
        }
        public override Style Style {
            get {
                return _style;
            }
            set {
                _style = value;
            }
        }
        #endregion

        #region Private fields
        private Dot start, end;
        private Point lastAsked;
        private bool _selected;
        private Style _style;
        private static Random gen = new Random();
        private bool _highlighted = false;
        #endregion
    }
}
