﻿/* There are currently to implementation of drawing:
 * A - the slow implementation, which, however, is quite soomth
 * B - the fast implementation, although it makes things to 'blink' while dynamicaly drawing
 * The difference between those two is that in implementation A objects are first drawed
 * to an image, and then that image is rendered on screen, whereas in implementation B objects
 * are drawn straight to screen.
 * Implementation A uses GPU, implementation B uses CPU.
 * In both cases, drawing isn't as pretty as i wish it would be, but i was not able to solve
 * this problem
 * */
// The following line turns on implementation B when uncommented:
//#define FAST_BUT_BLINKING
using Algorithms;
using Algorithms.Datastructures;
using Digitalis.GUI.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace UserInterface {
    enum UserMode { Select, DrawDot, DrawLine, Move };
    public partial class Editor : Form {
        public Editor() {
            InitializeComponent();
        }

        #region Inits
        private void Editor_Load(object sender, EventArgs e) {
            userMode = UserMode.DrawDot;
            view = new View();
            InitCanvas();
            InitToolTip();
            DisableEditTab();
            UpdateView();
            UpdateStatusLine();
            ShowMessage("Правий клік викликає вікно підказки.");
        }
        private void Editor_Shown(object sender, EventArgs e) {
            MessageBox.Show("Правий клік викликає вікно підказки.");
        }
        private void canvasBox_Resize(object sender, EventArgs e) {
            InitCanvas();
        }
        private void InitCanvas() {
#if FAST_BUT_BLINKING
            canvas = canvasBox.CreateGraphics();
#else
            image = new Bitmap(canvasBox.Width, canvasBox.Height);
            canvas = Graphics.FromImage(image);
#endif
            canvas.Clear(Color.White);
        }
        #endregion

        #region UserTips
        private void InitToolTip() {
            toolTip = new InteractiveToolTip();
            toolTip.ToolTipShown += TipShown;
            toolTip.ToolTipHidden += TipHiden;
        }
        private void TipShown(object sender, EventArgs args)
            => tipIsShown = true;
        private void TipHiden(object sender, EventArgs args)
            => tipIsShown = false;
        private void OnRightClick(object sender, MouseEventArgs args) {
            if (tipIsShown || args.Button != MouseButtons.Right)
                return;
            Control tip;
            if (sender is ToolStripMenuItem item) {
                tip = GetTipForMenuItem(item);
                var bounds = GetMenuItemBounds(item);
                toolTip.Show(tip, toolsMenuStrip,
                    bounds.X + bounds.Width / 5,
                    bounds.Y);
                return;
            }
            var control = sender as Control;
            if (control == null)
                return;
            tip = GetTipForControl(control);
            toolTip.Show(tip, control, args.X, args.Y);
        }
        private UserTips.UserTip GetTipForMenuItem(ToolStripMenuItem item) {
            if (item == viewToolStripMenuItem ||
                item == openToolStripMenuItem ||
                item == saveToolStripMenuItem) {
                return new UserTips.View_Tip(toolTip);
            }
            if (item == drawToolStripMenuItem) {
                return new UserTips.Draw_Tip(toolTip);
            }
            if (item == lineToolStripMenuItem) {
                return new UserTips.DrawLine_Tip(toolTip);
            }
            if (item == dotToolStripMenuItem) {
                return new UserTips.DrawDot_Tip(toolTip);
            }
            if (item == selectToolStripMenuItem) {
                return new UserTips.Select_Tip(toolTip);
            }
            if (item == moveToolStripMenuItem) {
                return new UserTips.Move_Tip(toolTip);
            }
            if (item == deleteToolStripMenuItem ||
                item == deleteAllToolStripMenuItem ||
                item == deleteSelectedToolStripMenuItem) {
                return new UserTips.Delete_Tip(toolTip);
            }
            if (item == navigateToolStripMenuItem ||
                item == toNavigatorMenuStripButton ||
                item == exitButtonMenuStrip) {
                return new UserTips.Navigate_Tip(toolTip);
            }
            return new UserTips.UserTip(toolTip);
        }
        private UserTips.UserTip GetTipForControl(Control control) {
            if (control == toolsMenuStrip) {
                return new UserTips.MenuStrip_Tip(toolTip);
            }
            if (control == canvasBox) {
                if (_result == null) {
                    return new UserTips.CanvasBox_Tip(toolTip);
                } else {
                    return new UserTips.FilledCanvasBox_Tip(toolTip);
                }
            }
            if (control == statusStrip) {
                return new UserTips.StatusStrip_Tip(toolTip);
            }
            if (control == myTabControl) {
                return new UserTips.TabControl_Tip(toolTip);
            }
            if (control == algoTab) {
                return new UserTips.BlankAlgoTab_Tip(toolTip);
            }
            if (control == algoTab ||
                control == algorithmInfoLabel ||
                control == cycle1List ||
                control == cycle2List) {
                if (_result == null) {
                    return new UserTips.BlankAlgoTab_Tip(toolTip);
                } else {
                    return new UserTips.FilledAlgoTab_Tip(toolTip);
                }
            }
            if (control == editTab ||
                control == editTab_WeightLabel ||
                control == editTab_applyButton ||
                control == editTab_WeightTextBox) {
                return new UserTips.EditTab_Tip(toolTip);
            }
            throw new ArgumentException("Such control is not supported", nameof(control));
        }
        private Rectangle GetMenuItemBounds(ToolStripMenuItem item) {
            if (item == viewToolStripMenuItem ||
                item == openToolStripMenuItem ||
                item == saveToolStripMenuItem) {
                return viewToolStripMenuItem.Bounds;
            }
            if (item == drawToolStripMenuItem ||
                item == lineToolStripMenuItem ||
                item == dotToolStripMenuItem) {
                return drawToolStripMenuItem.Bounds;
            }
            if (item == selectToolStripMenuItem) {
                return selectToolStripMenuItem.Bounds;
            }
            if (item == moveToolStripMenuItem) {
                return moveToolStripMenuItem.Bounds;
            }
            if (item == deleteToolStripMenuItem ||
                item == deleteAllToolStripMenuItem ||
                item == deleteSelectedToolStripMenuItem) {
                return deleteToolStripMenuItem.Bounds;
            }
            if (item == navigateToolStripMenuItem ||
                item == toNavigatorMenuStripButton ||
                item == exitButtonMenuStrip) {
                return navigateToolStripMenuItem.Bounds;
            }
            throw new ArgumentException("Such item is not supported", nameof(item));
        }
        #endregion

        #region ToolStripMenu clicks
        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            SaveFileDialog dialog = new SaveFileDialog() {
                InitialDirectory = default_path,
                Title = "Оберіть, де зберегти Ваш .toml файл.",
                DefaultExt = "toml",
                Filter = "TOML files (*.toml)|*.toml",
            };
            if (dialog.ShowDialog() == DialogResult.OK) {
                view.WriteToFile(dialog.FileName);
            }
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            _result = null;
            OpenFileDialog dialog = new OpenFileDialog() {
                InitialDirectory = default_path,
                Title = "Оберіть .toml файл з даними моделі.",
                CheckFileExists = true,
                CheckPathExists = true,
                DefaultExt = "toml",
                Filter = "TOML files (*.toml)|*.toml",
            };
            if (dialog.ShowDialog() == DialogResult.OK) {
                DropResult();
                var file = dialog.FileName;
                view.ReadFromFile(file);
                UpdateView();
            }
        }
        private void lineToolStripMenuItem_Click(object sender, EventArgs e) {
            ClearToolStripMenuSelection();
            userMode = UserMode.DrawLine;
            SelectToolStripMenuItem(lineToolStripMenuItem);
            UpdateStatusLine();
        }
        private void dotToolStripMenuItem_Click(object sender, EventArgs e) {
            ClearToolStripMenuSelection();
            userMode = UserMode.DrawDot;
            SelectToolStripMenuItem(dotToolStripMenuItem);
            UpdateStatusLine();
        }
        private void selectToolStripMenuItem_Click(object sender, EventArgs e) {
            ClearToolStripMenuSelection();
            userMode = UserMode.Select;
            SelectToolStripMenuItem(selectToolStripMenuItem);
            UpdateStatusLine();
        }
        private void moveToolStripMenuItem_Click(object sender, EventArgs e) {
            ClearToolStripMenuSelection();
            userMode = UserMode.Move;
            SelectToolStripMenuItem(moveToolStripMenuItem);
            UpdateStatusLine();
        }
        private void deleteSelectedToolStripMenuItem_Click(object sender, EventArgs e) {
            view.DeleteSelected();
            DropResult();
            UpdateView();
            UpdateStatusLine();
        }
        private void deleteAllToolStripMenuItem_Click(object sender, EventArgs e) {
            view.DeleteAll();
            DropResult();
            UpdateView();
            UpdateStatusLine();
        }
        private void ClearToolStripMenuSelection() {
            Color mainItemBack = Color.FromKnownColor(KnownColor.ControlDark);
            Color subItemBack = Color.White;
            switch (userMode) {
            case UserMode.Select: {
                selectToolStripMenuItem.BackColor = mainItemBack;
            }
            break;
            case UserMode.Move: {
                moveToolStripMenuItem.BackColor = mainItemBack;
            }
            break;
            case UserMode.DrawDot: {
                drawToolStripMenuItem.BackColor = mainItemBack;
                dotToolStripMenuItem.BackColor = subItemBack;
            }
            break;
            case UserMode.DrawLine: {
                drawToolStripMenuItem.BackColor = mainItemBack;
                lineToolStripMenuItem.BackColor = subItemBack;
            }
            break;
            default:
            break;
            }
        }
        private void SelectToolStripMenuItem(ToolStripMenuItem item) {
            Color mainItemSelection = Color.FromKnownColor(KnownColor.ControlDarkDark);
            Color subItemSelection = Color.FromKnownColor(KnownColor.ActiveCaption);
            if (item == dotToolStripMenuItem ||
                item == lineToolStripMenuItem) {
                drawToolStripMenuItem.BackColor = mainItemSelection;
                item.BackColor = subItemSelection;
                return;
            }
            if (item == moveToolStripMenuItem ||
                item == selectToolStripMenuItem) {
                item.BackColor = mainItemSelection;
            }
        }
        #endregion

        #region StatusLine Stuff
        private void UpdateStatusLine() {
            StatusLine_UserModeLabel.Text = $"Режим: {userMode}.";
        }
        private void ShowMessage(string messsage) {
            StatusLine_messageLabel.Text = messsage;
        }
        #endregion

        #region Edit Tab
        void DisableEditTab() {
            editTab_applyButton.Enabled = false;
            editTab_WeightLabel.Enabled = false;
            editTab_WeightTextBox.Enabled = false;
        }
        void EnableEditTab() {
            editTab_applyButton.Enabled = true;
            editTab_WeightLabel.Enabled = true;
            editTab_WeightTextBox.Enabled = true;
        }
        void On_editApplyButton(object sender, EventArgs args) {
            if (view.SelectedElement is Line line) {
                int new_weight = -1;
                bool invalid = false;
                try {
                    new_weight = Convert.ToInt32(editTab_WeightTextBox.Text);
                    invalid = new_weight < 1;
                } catch (FormatException) {
                    invalid = true;
                }
                if (invalid) {
                    ShowMessage("Введіть додатнє ціле значення");
                    editTab_WeightTextBox.Text = line.Weight.ToString();
                    return;
                }
                line.Weight = new_weight;
            }
            DropResult();
            UpdateView();
        }
        #endregion

        #region Algo Tab
        void OnStartAlgorithm(object sender, EventArgs args) {
            // gather input data
            var input = GetInput();
            // validate input data
            string message = ValidateInput(input);
            if (message != "") {
                algorithmInfoLabel.Text = message;
                return;
            }
            // run algorithm on gathered data if it was ok
            _result = PerformAlgorithm(input);
            // show result
            ShowResult();
            // draw the view
            UpdateView();
        }
        private TwoSalesmen.Result PerformAlgorithm(TwoSalesmen.Input input) {
            var a = new TwoSalesmen();
            a.SetInput(input);
            a.Start();
            return a.GetResult() as TwoSalesmen.Result;
        }
        private void chooserIndexChanged(object sender, EventArgs e) {
            int index1 = cycle1List.SelectedIndex;
            int index2 = cycle2List.SelectedIndex;
            if (index1 == -1 || index2 == -1 )//|| index1 >= _result.AllCycles.Count || index2 >= _result.AllCycles.Count)
            {
                // this happens when the lists are filled for the first time
                return;
            }
            var cycle1 = _result.AllCycles[index1];
            var cycle2 = _result.AllCycles[index2];
            var weight1 = _result.Weights[index1];
            var weight2 = _result.Weights[index2];
            view.ResetStyles();
            ViewCycles(cycle1, cycle2);
            cycle1Label.Text = $"Цикл 1: {CycleAsString(cycle1, weight1)}";
            cycle2Label.Text = $"Цикл 2: {CycleAsString(cycle2, weight2)}";
            //string message = $"Цикл 1: {CycleAsString(cycle1, weight1)}\nЦикл 2: {CycleAsString(cycle2, weight2)}\nЧас: Max({weight1}, {weight2})";
            //algorithmInfoLabel.Text = message;
            algorithmInfoLabel.Text = $"Загальний час: {Math.Max(weight1, weight2)}";
            UpdateView();
        }
        private string ValidateInput(TwoSalesmen.Input input) {
            if (input.TargetVertex < 0 || input.TargetVertex > input.WeightedMatrix.Size) {
                return "Оберіть початкову вершину.";
            }
            const int MAX_SIZE = sizeof(int) * 8;
            if (input.WeightedMatrix.Size > MAX_SIZE) {
                return $"Забагато вершин({input.WeightedMatrix.Size - MAX_SIZE }).";
            }
            foreach (var degree in new Graph(input.WeightedMatrix.AsMatrix).GetDegrees()) {
                if (degree < 2)
                    return "Ранг кожної вершини має бути більше одиниці.";
            }
            return "";
        }
        private TwoSalesmen.Input GetInput()
            => new TwoSalesmen.Input(view.ToWeightedMatrix(),
               view.Dots.IndexOf(view.SelectedElement as Dot));
        private void OnUpdateResult(object sender, EventArgs args) {
            ShowResult();
            UpdateView();
        }
        private void ShowResult() {
            if (_result == null) {
                algorithmInfoLabel.Text = "Немає збереженого результату. Запустіть алгоритм, будь-ласка.";
                return;
            }
            ViewCycles(_result.Cycle1, _result.Cycle2);
            DisplayCycles(cycle1List);
            cycle1List.SelectedIndex = _result.Index1;
            DisplayCycles(cycle2List);
            cycle2List.SelectedIndex = _result.Index2;
        }
        private void ViewCycles(List<int> cycle1, List<int> cycle2) {
            view.ResetStyles();
            view.HighlightPath(cycle1, Style.Cycle1);
            view.HighlightPath(cycle2, Style.Cycle2);
        }
        private void DisplayCycles(ListBox list) {
            if (_result == null) return;
            for (int i = 0; i < _result.AllCycles.Count; i++) {
                var cycle = _result.AllCycles[i];
                int weight = _result.Weights[i];
                list.Items.Add(CycleAsString(cycle, weight));
            }
        }
        private string CycleAsString(List<int> cycle, int? weight = null) {
            if (cycle == null) {
                return "нема";
            }
            string res = "";
            if (weight != null)
                res = $"{weight}: ";
            res += "{ ";
            for (int i = 0; i < cycle.Count; ++i) {
                res += cycle[i].ToString();
                if (i + 1 != cycle.Count) {
                    res += ", ";
                } else {
                    res += " ";
                }
            }
            res += "}";
            return res;
        }
        private void DropResult() {
            view.ResetStyles();
            _result = null;
            cycle1List.Items.Clear();
            cycle2List.Items.Clear();
            algorithmInfoLabel.Text = "Запустіть алгоритм.";
        }
        #endregion

        #region Handling mouse on canvas
        private void UpdateView() {
#if FAST_BUT_BLINKING
            drawingLine?.Draw(canvas);
            view.Draw(canvas);
#else
            canvas.Clear(Color.White);
            drawingLine?.Draw(canvas);
            view.Draw(canvas);
            canvasBox.Image = image;
#endif
            // tab control
            if (view.SelectedElement is Line line) {
                EnableEditTab();
                editTab_WeightTextBox.Text = line.Weight.ToString();
            } else {
                DisableEditTab();
            }
        }
        private void canvasBox_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button != MouseButtons.Left) {
                return;
            }
            switch (userMode) {
            case UserMode.DrawDot: {
                var startingPoint = e.Location;
                if (view.Contains(startingPoint) == null) {
                    view.AddNewDot(startingPoint);
                    DropResult();
                    ShowMessage("Створено вершину");
                } else {
                    ShowMessage("У даній точці вже існує вершина.");
                }
            }
            break;
            case UserMode.DrawLine: {
                foreach (var dot in view.Dots) {
                    if (dot.Contains(e.Location)) {
                        drawingLine = new Line(dot, new Dot(e.Location));
                        ShowMessage($"Ребро почато у X = {dot.X}, Y = {dot.Y}.");
                        break;
                    }
                }
                if (drawingLine == null) {
                    ShowMessage($"Вершину у даній точці не знайдено.");
                }
            }
            break;
            case UserMode.Select: {
                view.ResetStyles();
                view.Select(e.Location);
            }
            break;
            case UserMode.Move: {
                movingElement = view.TouchedBy(e.Location);
                if (movingElement != null)
                    ShowMessage($"Знайдено елемент для переміщення");
                else
                    ShowMessage("Не знайдено елемент для переміщення");
            }
            break;
            default: {
                //ShowMessage("Can't do it!");
            }
            break;
            }
            UpdateView();
            UpdateStatusLine();
        }
        private void canvasBox_MouseMove(object sender, MouseEventArgs e) {
            if (e.Button != MouseButtons.Left) {
                return;
            }
#if FAST_BUT_BLINKING
            view.Clear(canvas);
            drawingLine?.Clear(canvas);
#endif
            switch (userMode) {
            case UserMode.DrawLine: {
                if (drawingLine == null)
                    break;
                drawingLine.End.MoveTo(e.Location);
                ShowMessage($"Ребро закінчується у X = {e.X}, Y = {e.Y}.");
            }
            break;
            case UserMode.Move: {
                if (movingElement == null)
                    break;
                movingElement.MoveTo(e.Location);
                ShowMessage($"Елемент переміщено до X = {e.X}, Y = {e.Y}.");
            }
            break;
            default: {
                //ShowMessage("Can't do it!");
            }
            break;
            }
            UpdateView();
            UpdateStatusLine();
        }
        private void canvasBox_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button != MouseButtons.Left) {
                return;
            }
#if FAST_BUT_BLINKING
            view.Clear(canvas);
            drawingLine?.Clear(canvas);
#endif
            switch (userMode) {
            case UserMode.DrawLine: {
                if (drawingLine == null)
                    break;
                foreach (var dot in view.Dots) {
                    if (dot.Contains(e.Location)) {
                        drawingLine.End = dot;
                        if (view.AddNewLine(drawingLine)) {
                            DropResult();
                            ShowMessage("Створено нове ребро.");
                        } else {
                            ShowMessage("Ребро між з'єднаними вершинами вже існує.");
                        }
                        drawingLine = null;
                        break;
                    }
                }
                if (drawingLine != null) {
                    drawingLine = null;
                    ShowMessage("Не знайдено кінцевої вершини.");
                }
            }
            break;
            case UserMode.Move: {
                if (movingElement == null)
                    break;
                movingElement.MoveTo(e.Location);
                ShowMessage($"Елемент переміщено до X = {e.X}, Y = {e.Y}");
            }
            break;
            default: {
                //ShowMessage("Can't do it!");
            }
            break;
            }
            UpdateView();
            UpdateStatusLine();
        }
        #endregion

        #region Private Fields
        private UserMode userMode = UserMode.DrawDot;
        private View view;
        private Bitmap image;
        private Graphics canvas;
        private Line drawingLine;
        private ViewElement movingElement;
        private TwoSalesmen.Result _result;
        private string default_path = Application.StartupPath;
        private InteractiveToolTip toolTip;
        private bool tipIsShown = false;
        #endregion

        #region Navigation
        public void NavigatorButtonClick(object sender, EventArgs args) {
            OnGoToNavigator();
        }
        public void ExitButtonClick(object sender, EventArgs args) {
            Application.Exit();
        }
        public delegate void GoToNavigator();
        public event GoToNavigator OnGoToNavigator;
        public delegate void GoToDocs();
        public event GoToDocs OnGoToDocs;
        #endregion
    }
}
