using System;
using System.Windows.Forms;

namespace UserInterface {
    static class Program {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // Constructing children
            title = new Title();
            editor = new Editor();
            navigator = new Navigator();
            docs = new Docs();
            // Managing children
            /* Navigator */
            navigator.OnGoToTitle += GoToTitle;
            navigator.OnGoToDocs += GoToDocs;
            navigator.OnGoToEditor += GoToEditor;
            /* Title */
            // note: patched for predefense
            //title.OnGoToNavigator += GoToNavigator;
            title.OnGoToNavigator += GoToEditor;
            /* Docs */
            docs.OnGoToNavigator += GoToNavigator;
            docs.OnGoToEditor += GoToEditor;
            /* Editor */
            // note: patched for predefense
            //editor.OnGoToNavigator += GoToNavigator;
            editor.OnGoToNavigator += GoToTitle;
            editor.OnGoToDocs += GoToDocs;
            /* Exit */
            title.FormClosed += Exit;
            editor.FormClosed += Exit;
            navigator.FormClosed += Exit;
            docs.FormClosed += Exit;
            // The Call
            Application.Run(title);
            //Application.Run(docs);
            //Application.Run(editor);
        }

        #region Navigation
        public static void Exit(object sender, EventArgs args) {
            Application.Exit();
        }
        public static void HideAll() {
            title.Hide();
            editor.Hide();
            docs.Hide();
            navigator.Hide();
        }
        public static void GoToTitle() {
            HideAll();
            title.Show();
        }
        public static void GoToNavigator() {
            HideAll();
            navigator.Show();
        }
        public static void GoToDocs() {
            HideAll();
            docs.Show();
        }
        public static void GoToEditor() {
            HideAll();
            editor.Show();
        }
        #endregion

        #region Child forms
        private static Title title;
        private static Editor editor;
        private static Navigator navigator;
        private static Docs docs;
        #endregion
    }
}
