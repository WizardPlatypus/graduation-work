﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace UserInterface {
    /// <summary>
    /// Class representing Dot, aka Vertex in graph
    /// </summary>
    public class Dot : ViewElement {
        #region Constructors
        public Dot() {
            center = new Point(0, 0);
            radius = 0;
            Style = Style.Default;
        }
        public Dot(Point center, float radius = 15) {
            this.center = center;
            this.radius = radius;
            Style = Style.Default;
        }
        public Dot(int x, int y, float radius = 15)
            : this(new Point(x, y), radius) {
        }
        #endregion

        #region Drawing
        public override void Draw(Graphics g) {
            Resize();
            Rectangle bounds = new Rectangle() {
                X = center.X - (int)radius,
                Y = center.Y - (int)radius,
                Height = (int)(radius * 2),
                Width = (int)(radius * 2)
            };
            Style style = Style;
            // drawing dot
            Brush fill = new SolidBrush(style.FillColor);
            Pen Peter = new Pen(style.StrokeColor, style.StrokeWidth);
            g.FillEllipse(fill, bounds);
            g.DrawEllipse(Peter, bounds);
            // drawing text
            SharedMethods.DrawText(g, Index.ToString(), style.TextFont, style.TextColor, bounds);
        }
        public override void Clear(Graphics g) {
            var style = Style.Blank;
            int overlap = 2;
            Rectangle bounds = new Rectangle() {
                X = center.X - (int)radius - overlap,
                Y = center.Y - (int)radius - overlap,
                Height = (int)((radius + overlap) * 2),
                Width = (int)((radius + overlap) * 2)
            };
            Brush fill = new SolidBrush(style.FillColor);
            g.FillEllipse(fill, bounds);
        }
        #endregion

        #region Attitudes
        public override bool Contains(Point point) {
            int distance, dx, dy;
            dx = center.X - point.X;
            dy = center.Y - point.Y;
            distance = dx * dx + dy * dy;
            // R^2 = dx^2 + dy^2: point is on the edge
            // R^2 > dx^2 + dy^2: point is inside
            // R^2 < dx^2 + dy^2: point is outside
            return distance <= radius * radius;
        }
        public override bool isContainedBy(Rectangle rectangle) {
            return
                center.X - radius >= rectangle.X &&
                center.Y - radius >= rectangle.Y &&
                center.X + radius <= rectangle.X + rectangle.Width &&
                center.Y + radius <= rectangle.Y + rectangle.Height;
        }
        public override bool TouchedBy(Point point) {
            return Contains(point);
        }
        #endregion

        #region State flexibility
        public override bool MoveTo(Point position) {
            center.X = position.X;
            center.Y = position.Y;
            return true;
        }
        public override void ResetStyle() {
            _highlighted = false;
            if (_selected) {
                _style = Style.Selected;
            } else {
                _style = Style.Default;
            }
        }
        public override void Highlight(Style style) {
            if (!_highlighted) {
                Style = style;
                _highlighted = true;
                return;
            }
            Style += style;
        }
        private void Resize() {
            var textSize = TextRenderer.MeasureText(Index.ToString(), Style.TextFont);
            float margin = 2f;
            radius = Math.Max(textSize.Width, textSize.Height) / 2f + margin;
        }
        #endregion

        #region Public Properties
        public int X { get => center.X; set => center.X = value; }
        public int Y { get => center.Y; set => center.Y = value; }
        public float Radius { get => radius; set => radius = value; }
        public int Index { get; set; }
        public Point Center { get => center; }
        public override bool Selected {
            get => _selected;
            set {
                _selected = value;
                ResetStyle();
            }
        }
        public override Style Style {
            get {
                return _style;
            }
            set {
                _style = value;
            }
        }
        #endregion

        #region Private Fields
        private Point center;
        private float radius;
        private bool _selected;
        private Style _style;
        private bool _highlighted;
        #endregion
    }
}
