﻿using System.Collections.Generic;
using System.IO;
using System.Drawing;
using Algorithms.Datastructures;
using Tommy;

namespace UserInterface {
    public class View {
        #region Drawing
        public void Draw(Graphics g) {
            // draw lines
            foreach (var line in lines.GetValues()) {
                line?.Draw(g);
            }
            // draw dots
            foreach (var dot in dots) {
                dot.Draw(g);
            }
        }
        public void Clear(Graphics g) {
            // draw lines
            foreach (var line in lines.GetValues()) {
                line?.Clear(g);
            }
            // draw dots
            foreach (var dot in dots) {
                dot.Clear(g);
            }
        }
        public void ResetStyles() {
            foreach (var e in dots) {
                e.ResetStyle();
            }
            foreach (var e in lines.GetValues()) {
                e?.ResetStyle();
            }
        }
        #endregion

        #region Containment
        public ViewElement Contains(Point point) {
            foreach (var element in dots) {
                if (element.Contains(point)) {
                    return element;
                }
            }
            foreach (var element in lines.GetValues()) {
                if (element == null)
                    continue;
                if (element.Contains(point)) {
                    return element;
                }
            }
            return null;
        }
        public ViewElement TouchedBy(Point point) {
            foreach (var element in dots) {
                if (element.TouchedBy(point)) {
                    return element;
                }
            }
            foreach (var element in lines.GetValues()) {
                if (element == null)
                    continue;
                if (element.TouchedBy(point)) {
                    return element;
                }
            }
            return null;
        }
        public Line Connection(Dot a, Dot b) {
            return lines[a.Index, b.Index];
        }
        #endregion

        #region Add
        public void AddNewDot(Point point) {
            dots.Add(new Dot(point) { Index = dots.Count });
            lines.Grow(1);
        }
        public bool AddNewLine(in Dot start, in Dot end) {
            int i = start.Index, j = end.Index;
            if (lines[i, j] is Line)
                return false;
            var line = new Line(start, end);
            line.Randomize();
            lines[i, j] = line;
            return true;
        }
        public bool AddNewLine(Line line) {
            return AddNewLine(line.Start, line.End);
        }
        #endregion

        #region Delete
        public void Delete(ViewElement viewElement) {
            if (viewElement is Dot dot) {
                int i = dot.Index;
                if (SelectedElement is Line selected_line) {
                    if (selected_line.Start == dot || selected_line.End == dot) {
                        SelectedElement = null;
                    }
                }
                dots.RemoveAt(i);
                lines.Delete(i);
                for (int j = i; j < dots.Count; j++) {
                    dots[j].Index = j;
                }
                return;
            }
            if (viewElement is Line line) {
                if (SelectedElement == line) {
                    SelectedElement = null;
                }
                lines[line.Start.Index, line.End.Index] = null;
                return;
            }
        }
        public void DeleteSelected() {
            Delete(SelectedElement);
        }
        public void DeleteAll() {
            SelectedElement = null;
            dots.Clear();
            lines = new FlatMatrix<Line>();
        }
        #endregion

        #region Selection
        public void Select(Point point) {
            // one selected element at a time
            var victim = TouchedBy(point);
            if (victim == null)
                return;
            if (victim == SelectedElement) // which means it is selected
            {
                SelectedElement.Selected = false;
                SelectedElement = null;
                return;
            } else {
                if (SelectedElement != null)
                    SelectedElement.Selected = false;
                SelectedElement = victim;
                SelectedElement.Selected = true;
                return;
            }
        }
        public void ClearSelection() {
            if (SelectedElement == null)
                return;
            SelectedElement.Selected = false;
            SelectedElement = null;
        }
        public void HighlightPath(List<int> path, Style style) {
            if (path == null) return;
            // first number shall not be highlighted twice
            for (int j = 1; j < path.Count; j++) {
                int i = path[j];
                dots[i].Highlight(style);
            }
            for (int i = 0; i + 1 < path.Count; i++) {
                if (lines[dots[path[i]].Index, dots[path[i + 1]].Index] is Line line) {
                    //line.Style = style;
                    line.Highlight(style);
                }
            }
        }
        #endregion

        #region Convertions
        public TomlTable ConvertToToml() {
            TomlArray toml_dots = new TomlArray { };
            foreach (var dot in dots) {
                toml_dots.Add(new TomlArray { dot.X, dot.Y });
            }
            TomlArray toml_lines = new TomlArray { };
            foreach (var line in lines.GetValues()) {
                if (line == null) continue;
                toml_lines.Add(new TomlArray { line.Start.Index, line.End.Index, line.Weight });
            }
            TomlTable toml_view = new TomlTable {
                ["dots"] = toml_dots,
                ["lines"] = toml_lines,
            };
            return toml_view;
        }
        public void ConvertFromToml(TomlTable toml_view) {
            DeleteAll();
            dots = new List<Dot>();
            foreach (TomlArray e in toml_view["dots"]) {
                var dot = new Dot(e[0], e[1]);
                dot.Index = dots.Count;
                dots.Add(dot);
            }
            lines = new FlatMatrix<Line>(dots.Count);
            foreach (TomlArray e in toml_view["lines"]) {
                int i = e[0], j = e[1], w = e[2];
                lines[i, j] = new Line(dots[i], dots[j]);
                lines[i, j].Weight = w;
            }
        }
        public WeightedMatrix ToWeightedMatrix() {
            var matrix = new WeightedMatrix(dots.Count);
            foreach (var line in lines.GetValues()) {
                if (line == null) continue;
                matrix[line.Start.Index, line.End.Index] = line.Weight;
            }
            return matrix;
        }
        #endregion

        #region Write/Read
        public void WriteToFile(string path) {
            var toml_view = ConvertToToml();
            using (StreamWriter writer = File.CreateText(path)) {
                toml_view.WriteTo(writer);
                writer.Flush();
            }
        }
        public void ReadFromFile(string path) {
            using (StreamReader reader = File.OpenText(path)) {
                TomlTable toml_view = TOML.Parse(reader);
                ConvertFromToml(toml_view);
            }
        }
        #endregion

        #region Public properties
        public List<Dot> Dots { get => dots; }
        public List<Line> Lines { get => lines.GetValues(); }
        public ViewElement SelectedElement { get; set; }
        #endregion

        #region Private fields
        private List<Dot> dots = new List<Dot>();
        private FlatMatrix<Line> lines = new FlatMatrix<Line>();
        #endregion
    }
}
