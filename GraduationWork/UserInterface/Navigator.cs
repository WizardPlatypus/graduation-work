﻿using System.Windows.Forms;

namespace UserInterface {
    public partial class Navigator : Form {
        public Navigator() {
            InitializeComponent();
        }

        #region Navigation
        public delegate void GoToTitle();
        public event GoToTitle OnGoToTitle;
        public delegate void GoToEditor();
        public event GoToEditor OnGoToEditor;
        public delegate void GoToDocs();
        public event GoToDocs OnGoToDocs;
        #endregion
    }
}
