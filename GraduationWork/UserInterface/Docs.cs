﻿using System.Windows.Forms;

namespace UserInterface {
    public partial class Docs : Form {
        public Docs() {
            InitializeComponent();
        }

        #region Navigation
        public delegate void GoToNavigator();
        public event GoToNavigator OnGoToNavigator;
        public delegate void GoToEditor();
        public event GoToEditor OnGoToEditor;
        #endregion
    }
}
