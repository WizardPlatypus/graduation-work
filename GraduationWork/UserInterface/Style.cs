﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace UserInterface {
    public class Style {
        #region Static members
        public readonly static Style Default = new Style() {
            FillColor = Color.White,
            StrokeColor = Color.Black,
            TextColor = Color.Black,
            DashStyle = DashStyle.Dot
        };
        public readonly static Style Selected = new Style() {
            FillColor = Color.LightYellow,
            StrokeColor = Color.Orange,
            TextColor = Color.DarkOrange,
        };
        public readonly static Style Cycle1 = new Style() {
            FillColor = Color.FromArgb(255, 191, 191, 255), // light blue
            StrokeColor = Color.FromArgb(255, 0, 0, 255), // blue
            TextColor = Color.DarkBlue,
        };
        public readonly static Style Cycle2 = new Style() {
            FillColor = Color.FromArgb(255, 255, 191, 191), // light red
            StrokeColor = Color.FromArgb(255, 255, 0, 0), // red
            TextColor = Color.DarkRed,
        };
        public readonly static Style Blank = new Style() {
            FillColor = Color.White,
            StrokeColor = Color.White,
            TextColor = Color.White,
            StrokeWidth = 0.0f
        };
        #endregion

        #region Public Properties
        public Color FillColor { get; set; }
        public Color StrokeColor { get; set; }
        public Color TextColor { get; set; }
        public Font TextFont { get; set; } = new Font("Arial", 13.0f);
        public float StrokeWidth { get; set; } = 4;
        public DashStyle DashStyle { get; set; } = DashStyle.Solid;
        #endregion

        #region Operators
        public static Style operator +(Style a, Style b) {
            Style c = new Style();
            c.FillColor = Mix(a.FillColor, b.FillColor);
            c.StrokeColor = Mix(a.StrokeColor, b.StrokeColor);
            c.TextColor = Mix(a.TextColor, b.TextColor);
            c.TextFont = a.TextFont;
            c.StrokeWidth = (a.StrokeWidth + b.StrokeWidth) / 2;
            return c;
        }
        public static Color Mix(Color a, Color b) {
            int A, R, G, B;
            A = (a.A + b.A) / 2;
            R = (a.R + b.R) / 2;
            G = (a.R + b.G) / 2;
            B = (a.B + b.B) / 2;
            return Color.FromArgb(A, R, G, B);
        }
        #endregion
    }
}
