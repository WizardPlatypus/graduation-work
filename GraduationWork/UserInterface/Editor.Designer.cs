﻿
namespace UserInterface
{
    partial class Editor
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolsMenuStrip = new System.Windows.Forms.MenuStrip();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toNavigatorMenuStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.exitButtonMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusLine_messageLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusLine_UserModeLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutBackfill = new System.Windows.Forms.TableLayoutPanel();
            this.myTabControl = new System.Windows.Forms.TabControl();
            this.algoTab = new System.Windows.Forms.TabPage();
            this.cycleChooserPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cycle1Label = new System.Windows.Forms.Label();
            this.algorithmInfoLabel = new System.Windows.Forms.Label();
            this.cycle2List = new System.Windows.Forms.ListBox();
            this.cycle1List = new System.Windows.Forms.ListBox();
            this.cycle2Label = new System.Windows.Forms.Label();
            this.startAlgorithmButton = new System.Windows.Forms.Button();
            this.editTab = new System.Windows.Forms.TabPage();
            this.editTab_applyButton = new System.Windows.Forms.Button();
            this.editTab_WeightTextBox = new System.Windows.Forms.TextBox();
            this.editTab_WeightLabel = new System.Windows.Forms.Label();
            this.canvasBox = new System.Windows.Forms.PictureBox();
            this.toolsMenuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.tableLayoutBackfill.SuspendLayout();
            this.myTabControl.SuspendLayout();
            this.algoTab.SuspendLayout();
            this.cycleChooserPanel.SuspendLayout();
            this.editTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canvasBox)).BeginInit();
            this.SuspendLayout();
            // 
            // toolsMenuStrip
            // 
            this.toolsMenuStrip.BackColor = System.Drawing.SystemColors.ControlDark;
            this.toolsMenuStrip.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.toolsMenuStrip.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.toolsMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.drawToolStripMenuItem,
            this.selectToolStripMenuItem,
            this.moveToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.navigateToolStripMenuItem});
            this.toolsMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.toolsMenuStrip.Name = "toolsMenuStrip";
            this.toolsMenuStrip.Size = new System.Drawing.Size(1171, 41);
            this.toolsMenuStrip.TabIndex = 0;
            this.toolsMenuStrip.Text = "menuStrip1";
            this.toolsMenuStrip.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(117, 37);
            this.viewToolStripMenuItem.Text = "Модель";
            this.viewToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(220, 42);
            this.saveToolStripMenuItem.Text = "Зберегти";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            this.saveToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(220, 42);
            this.openToolStripMenuItem.Text = "Відкрити";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            this.openToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // drawToolStripMenuItem
            // 
            this.drawToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lineToolStripMenuItem,
            this.dotToolStripMenuItem});
            this.drawToolStripMenuItem.Name = "drawToolStripMenuItem";
            this.drawToolStripMenuItem.Size = new System.Drawing.Size(161, 37);
            this.drawToolStripMenuItem.Text = "Малювання";
            this.drawToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // lineToolStripMenuItem
            // 
            this.lineToolStripMenuItem.Name = "lineToolStripMenuItem";
            this.lineToolStripMenuItem.Size = new System.Drawing.Size(220, 42);
            this.lineToolStripMenuItem.Text = "Ребро";
            this.lineToolStripMenuItem.Click += new System.EventHandler(this.lineToolStripMenuItem_Click);
            this.lineToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // dotToolStripMenuItem
            // 
            this.dotToolStripMenuItem.Name = "dotToolStripMenuItem";
            this.dotToolStripMenuItem.Size = new System.Drawing.Size(220, 42);
            this.dotToolStripMenuItem.Text = "Вершина";
            this.dotToolStripMenuItem.Click += new System.EventHandler(this.dotToolStripMenuItem_Click);
            this.dotToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // selectToolStripMenuItem
            // 
            this.selectToolStripMenuItem.Name = "selectToolStripMenuItem";
            this.selectToolStripMenuItem.Size = new System.Drawing.Size(145, 37);
            this.selectToolStripMenuItem.Text = "Виділення";
            this.selectToolStripMenuItem.Click += new System.EventHandler(this.selectToolStripMenuItem_Click);
            this.selectToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // moveToolStripMenuItem
            // 
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            this.moveToolStripMenuItem.Size = new System.Drawing.Size(180, 37);
            this.moveToolStripMenuItem.Text = "Переміщення";
            this.moveToolStripMenuItem.Click += new System.EventHandler(this.moveToolStripMenuItem_Click);
            this.moveToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSelectedToolStripMenuItem,
            this.deleteAllToolStripMenuItem});
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(153, 37);
            this.deleteToolStripMenuItem.Text = "Видалення";
            this.deleteToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // deleteSelectedToolStripMenuItem
            // 
            this.deleteSelectedToolStripMenuItem.Name = "deleteSelectedToolStripMenuItem";
            this.deleteSelectedToolStripMenuItem.Size = new System.Drawing.Size(332, 42);
            this.deleteSelectedToolStripMenuItem.Text = "Видалити виділене";
            this.deleteSelectedToolStripMenuItem.Click += new System.EventHandler(this.deleteSelectedToolStripMenuItem_Click);
            this.deleteSelectedToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // deleteAllToolStripMenuItem
            // 
            this.deleteAllToolStripMenuItem.Name = "deleteAllToolStripMenuItem";
            this.deleteAllToolStripMenuItem.Size = new System.Drawing.Size(332, 42);
            this.deleteAllToolStripMenuItem.Text = "Видалити усе";
            this.deleteAllToolStripMenuItem.Click += new System.EventHandler(this.deleteAllToolStripMenuItem_Click);
            this.deleteAllToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // navigateToolStripMenuItem
            // 
            this.navigateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toNavigatorMenuStripButton,
            this.exitButtonMenuStrip});
            this.navigateToolStripMenuItem.Name = "navigateToolStripMenuItem";
            this.navigateToolStripMenuItem.Size = new System.Drawing.Size(133, 37);
            this.navigateToolStripMenuItem.Text = "Навігація";
            this.navigateToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // toNavigatorMenuStripButton
            // 
            this.toNavigatorMenuStripButton.Name = "toNavigatorMenuStripButton";
            this.toNavigatorMenuStripButton.Size = new System.Drawing.Size(367, 42);
            this.toNavigatorMenuStripButton.Text = "До Титульної Сторінки";
            this.toNavigatorMenuStripButton.Click += new System.EventHandler(this.NavigatorButtonClick);
            this.toNavigatorMenuStripButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // exitButtonMenuStrip
            // 
            this.exitButtonMenuStrip.Name = "exitButtonMenuStrip";
            this.exitButtonMenuStrip.Size = new System.Drawing.Size(367, 42);
            this.exitButtonMenuStrip.Text = "Вихід";
            this.exitButtonMenuStrip.Click += new System.EventHandler(this.ExitButtonClick);
            this.exitButtonMenuStrip.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // statusStrip
            // 
            this.statusStrip.BackColor = System.Drawing.SystemColors.ControlDark;
            this.statusStrip.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLine_messageLabel,
            this.splitLabel1,
            this.StatusLine_UserModeLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 597);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1171, 40);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip";
            this.statusStrip.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // StatusLine_messageLabel
            // 
            this.StatusLine_messageLabel.Name = "StatusLine_messageLabel";
            this.StatusLine_messageLabel.Size = new System.Drawing.Size(112, 33);
            this.StatusLine_messageLabel.Text = "Message";
            // 
            // splitLabel1
            // 
            this.splitLabel1.Name = "splitLabel1";
            this.splitLabel1.Size = new System.Drawing.Size(21, 33);
            this.splitLabel1.Text = "|";
            // 
            // StatusLine_UserModeLabel
            // 
            this.StatusLine_UserModeLabel.Name = "StatusLine_UserModeLabel";
            this.StatusLine_UserModeLabel.Size = new System.Drawing.Size(125, 33);
            this.StatusLine_UserModeLabel.Text = "UserMode";
            // 
            // tableLayoutBackfill
            // 
            this.tableLayoutBackfill.ColumnCount = 2;
            this.tableLayoutBackfill.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72F));
            this.tableLayoutBackfill.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableLayoutBackfill.Controls.Add(this.myTabControl, 1, 0);
            this.tableLayoutBackfill.Controls.Add(this.canvasBox, 0, 0);
            this.tableLayoutBackfill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutBackfill.Location = new System.Drawing.Point(0, 41);
            this.tableLayoutBackfill.Name = "tableLayoutBackfill";
            this.tableLayoutBackfill.RowCount = 1;
            this.tableLayoutBackfill.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutBackfill.Size = new System.Drawing.Size(1171, 556);
            this.tableLayoutBackfill.TabIndex = 4;
            // 
            // myTabControl
            // 
            this.myTabControl.Controls.Add(this.algoTab);
            this.myTabControl.Controls.Add(this.editTab);
            this.myTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.myTabControl.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.myTabControl.Location = new System.Drawing.Point(846, 3);
            this.myTabControl.Name = "myTabControl";
            this.myTabControl.SelectedIndex = 0;
            this.myTabControl.Size = new System.Drawing.Size(322, 550);
            this.myTabControl.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.myTabControl.TabIndex = 3;
            this.myTabControl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // algoTab
            // 
            this.algoTab.BackColor = System.Drawing.Color.LightGray;
            this.algoTab.Controls.Add(this.cycleChooserPanel);
            this.algoTab.Controls.Add(this.startAlgorithmButton);
            this.algoTab.Location = new System.Drawing.Point(4, 42);
            this.algoTab.Name = "algoTab";
            this.algoTab.Padding = new System.Windows.Forms.Padding(3);
            this.algoTab.Size = new System.Drawing.Size(314, 504);
            this.algoTab.TabIndex = 1;
            this.algoTab.Text = "Алгоритм";
            // 
            // cycleChooserPanel
            // 
            this.cycleChooserPanel.ColumnCount = 1;
            this.cycleChooserPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cycleChooserPanel.Controls.Add(this.cycle1Label, 0, 1);
            this.cycleChooserPanel.Controls.Add(this.algorithmInfoLabel, 0, 0);
            this.cycleChooserPanel.Controls.Add(this.cycle2List, 0, 4);
            this.cycleChooserPanel.Controls.Add(this.cycle1List, 0, 2);
            this.cycleChooserPanel.Controls.Add(this.cycle2Label, 0, 3);
            this.cycleChooserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cycleChooserPanel.Location = new System.Drawing.Point(3, 46);
            this.cycleChooserPanel.Name = "cycleChooserPanel";
            this.cycleChooserPanel.RowCount = 5;
            this.cycleChooserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.cycleChooserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.cycleChooserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cycleChooserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.cycleChooserPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cycleChooserPanel.Size = new System.Drawing.Size(308, 455);
            this.cycleChooserPanel.TabIndex = 4;
            // 
            // cycle1Label
            // 
            this.cycle1Label.AutoSize = true;
            this.cycle1Label.Location = new System.Drawing.Point(3, 39);
            this.cycle1Label.Name = "cycle1Label";
            this.cycle1Label.Size = new System.Drawing.Size(97, 33);
            this.cycle1Label.TabIndex = 5;
            this.cycle1Label.Text = "Цикл 1:";
            // 
            // algorithmInfoLabel
            // 
            this.algorithmInfoLabel.AutoSize = true;
            this.algorithmInfoLabel.Location = new System.Drawing.Point(3, 0);
            this.algorithmInfoLabel.Name = "algorithmInfoLabel";
            this.algorithmInfoLabel.Size = new System.Drawing.Size(139, 39);
            this.algorithmInfoLabel.TabIndex = 3;
            this.algorithmInfoLabel.Text = "Інформація";
            this.algorithmInfoLabel.UseCompatibleTextRendering = true;
            this.algorithmInfoLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // cycle2List
            // 
            this.cycle2List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cycle2List.FormattingEnabled = true;
            this.cycle2List.ItemHeight = 33;
            this.cycle2List.Location = new System.Drawing.Point(3, 283);
            this.cycle2List.Name = "cycle2List";
            this.cycle2List.Size = new System.Drawing.Size(302, 169);
            this.cycle2List.TabIndex = 1;
            this.cycle2List.SelectedIndexChanged += new System.EventHandler(this.chooserIndexChanged);
            this.cycle2List.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // cycle1List
            // 
            this.cycle1List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cycle1List.FormattingEnabled = true;
            this.cycle1List.ItemHeight = 33;
            this.cycle1List.Location = new System.Drawing.Point(3, 75);
            this.cycle1List.Name = "cycle1List";
            this.cycle1List.Size = new System.Drawing.Size(302, 169);
            this.cycle1List.TabIndex = 0;
            this.cycle1List.SelectedIndexChanged += new System.EventHandler(this.chooserIndexChanged);
            this.cycle1List.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // cycle2Label
            // 
            this.cycle2Label.AutoSize = true;
            this.cycle2Label.Location = new System.Drawing.Point(3, 247);
            this.cycle2Label.Name = "cycle2Label";
            this.cycle2Label.Size = new System.Drawing.Size(97, 33);
            this.cycle2Label.TabIndex = 6;
            this.cycle2Label.Text = "Цикл 2:";
            // 
            // startAlgorithmButton
            // 
            this.startAlgorithmButton.AutoSize = true;
            this.startAlgorithmButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.startAlgorithmButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.startAlgorithmButton.Location = new System.Drawing.Point(3, 3);
            this.startAlgorithmButton.Name = "startAlgorithmButton";
            this.startAlgorithmButton.Size = new System.Drawing.Size(308, 43);
            this.startAlgorithmButton.TabIndex = 2;
            this.startAlgorithmButton.Text = "Запустити";
            this.startAlgorithmButton.UseVisualStyleBackColor = true;
            this.startAlgorithmButton.Click += new System.EventHandler(this.OnStartAlgorithm);
            // 
            // editTab
            // 
            this.editTab.BackColor = System.Drawing.Color.LightGray;
            this.editTab.Controls.Add(this.editTab_applyButton);
            this.editTab.Controls.Add(this.editTab_WeightTextBox);
            this.editTab.Controls.Add(this.editTab_WeightLabel);
            this.editTab.Location = new System.Drawing.Point(4, 42);
            this.editTab.Name = "editTab";
            this.editTab.Padding = new System.Windows.Forms.Padding(3);
            this.editTab.Size = new System.Drawing.Size(314, 504);
            this.editTab.TabIndex = 0;
            this.editTab.Text = "Зміни";
            this.editTab.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // editTab_applyButton
            // 
            this.editTab_applyButton.AutoSize = true;
            this.editTab_applyButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editTab_applyButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.editTab_applyButton.Location = new System.Drawing.Point(3, 76);
            this.editTab_applyButton.Name = "editTab_applyButton";
            this.editTab_applyButton.Size = new System.Drawing.Size(308, 43);
            this.editTab_applyButton.TabIndex = 2;
            this.editTab_applyButton.Text = "Застосувати зміни";
            this.editTab_applyButton.UseVisualStyleBackColor = true;
            this.editTab_applyButton.Click += new System.EventHandler(this.On_editApplyButton);
            this.editTab_applyButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // editTab_WeightTextBox
            // 
            this.editTab_WeightTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.editTab_WeightTextBox.Location = new System.Drawing.Point(3, 36);
            this.editTab_WeightTextBox.Name = "editTab_WeightTextBox";
            this.editTab_WeightTextBox.Size = new System.Drawing.Size(308, 40);
            this.editTab_WeightTextBox.TabIndex = 1;
            // 
            // editTab_WeightLabel
            // 
            this.editTab_WeightLabel.AutoSize = true;
            this.editTab_WeightLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.editTab_WeightLabel.Location = new System.Drawing.Point(3, 3);
            this.editTab_WeightLabel.Name = "editTab_WeightLabel";
            this.editTab_WeightLabel.Size = new System.Drawing.Size(146, 33);
            this.editTab_WeightLabel.TabIndex = 0;
            this.editTab_WeightLabel.Text = "Вага ребра:";
            this.editTab_WeightLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            // 
            // canvasBox
            // 
            this.canvasBox.BackColor = System.Drawing.Color.White;
            this.canvasBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canvasBox.Location = new System.Drawing.Point(3, 3);
            this.canvasBox.Name = "canvasBox";
            this.canvasBox.Size = new System.Drawing.Size(837, 550);
            this.canvasBox.TabIndex = 1;
            this.canvasBox.TabStop = false;
            this.canvasBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnRightClick);
            this.canvasBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvasBox_MouseDown);
            this.canvasBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvasBox_MouseMove);
            this.canvasBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvasBox_MouseUp);
            this.canvasBox.Resize += new System.EventHandler(this.canvasBox_Resize);
            // 
            // Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1171, 637);
            this.Controls.Add(this.tableLayoutBackfill);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolsMenuStrip);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.MainMenuStrip = this.toolsMenuStrip;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Editor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактор";
            this.Load += new System.EventHandler(this.Editor_Load);
            this.Shown += new System.EventHandler(this.Editor_Shown);
            this.toolsMenuStrip.ResumeLayout(false);
            this.toolsMenuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tableLayoutBackfill.ResumeLayout(false);
            this.myTabControl.ResumeLayout(false);
            this.algoTab.ResumeLayout(false);
            this.algoTab.PerformLayout();
            this.cycleChooserPanel.ResumeLayout(false);
            this.cycleChooserPanel.PerformLayout();
            this.editTab.ResumeLayout(false);
            this.editTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canvasBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip toolsMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusLine_UserModeLabel;
        private System.Windows.Forms.ToolStripStatusLabel splitLabel1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLine_messageLabel;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteAllToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutBackfill;
        private System.Windows.Forms.PictureBox canvasBox;
        private System.Windows.Forms.ToolStripMenuItem navigateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toNavigatorMenuStripButton;
        private System.Windows.Forms.ToolStripMenuItem exitButtonMenuStrip;
        private System.Windows.Forms.TabControl myTabControl;
        private System.Windows.Forms.TabPage editTab;
        private System.Windows.Forms.Button editTab_applyButton;
        private System.Windows.Forms.TextBox editTab_WeightTextBox;
        private System.Windows.Forms.Label editTab_WeightLabel;
        private System.Windows.Forms.TabPage algoTab;
        private System.Windows.Forms.TableLayoutPanel cycleChooserPanel;
        private System.Windows.Forms.ListBox cycle2List;
        private System.Windows.Forms.ListBox cycle1List;
        private System.Windows.Forms.Label algorithmInfoLabel;
        private System.Windows.Forms.Button startAlgorithmButton;
        private System.Windows.Forms.Label cycle1Label;
        private System.Windows.Forms.Label cycle2Label;
    }
}

