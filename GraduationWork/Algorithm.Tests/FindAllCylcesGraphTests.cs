﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Datastructures;
using Xunit;

namespace Algorithms.Tests {
    public class FindAllCylcesGraphTests {
        static int GetTargetVertexByNumber(int number) {
            int target;
            switch (number) {
            case 0:
            default: {
                target = 0;
            }
            break;
            }
            return target;
        }
        static AdjacencyMatrix GetMatrixByNumber(int number) {
            AdjacencyMatrix matrix = null;
            switch (number) {
            case 0:
            default: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                            { false,  true,  true,  true, false }, // 0 1 1 1 0
                            {  true, false,  true, false, false }, // 1 0 1 0 0
                            {  true,  true, false,  true,  true }, // 1 1 0 1 1
                            {  true, false,  true, false,  true }, // 1 0 1 0 1
                            { false, false,  true,  true, false }, // 0 0 1 1 0
                });
            }
            break;
            }
            return matrix;
        }
        static List<List<int>> GetActualCycles(int number) {
            var matrix = GetMatrixByNumber(number);
            var graph = new Graph(matrix);
            var target_vertex = GetTargetVertexByNumber(number);
            var algorithm = new FindAllCyclesGraph();
            algorithm.SetInput(new FindAllCyclesGraph.Input(graph, target_vertex));
            algorithm.Start();
            var result = algorithm.GetResult() as FindAllCyclesGraph.Result;
            return result.Cycles;
        }
        static List<List<int>> GetExpectedCycles(int number) {
            List<List<int>> cycles = null;
            switch (number) {
            case 0:
            default: {
                cycles = new List<List<int>>
                {
                            new List<int>() { 0, 1, 2, 0 },
                            new List<int>() { 0, 1, 2, 3, 0 },
                            new List<int>() { 0, 1, 2, 3, 4, 2, 0 },
                            new List<int>() { 0, 1, 2, 4, 3, 0 },
                            new List<int>() { 0, 1, 2, 4, 3, 2, 0 },
                            new List<int>() { 0, 2, 3, 0 },
                            new List<int>() { 0, 2, 4, 3, 0 },
                        };
            }
            break;
            }
            return cycles;
        }

        [Theory]
        [InlineData(0)]
        void CyclesShouldContainTargetVertex(int number) {
            // When given: actual cycles, target vertex
            var actual_cycles = GetActualCycles(number);
            var target_vertex = GetTargetVertexByNumber(number);
            // Then: cycles must contain target vertex
            foreach (var cylce in actual_cycles) {
                Assert.True(cylce.First() == target_vertex, "cycle doesn't start with target vertex");
                Assert.True(cylce.Last() == target_vertex, "cycle doesn't end with target vertex");
            }
        }
        [Theory]
        [InlineData(0)]
        void CylcesShouldMatchTheAnswer(int number) {
            // When given: actual cycles, expected cycles
            var actual_cycles = GetActualCycles(number);
            var expected_cycles = GetExpectedCycles(number);
            // Then: actual cycles must match expected cycles
            Assert.Equal(expected_cycles.Count, actual_cycles.Count);
            foreach (var actual_cycle in actual_cycles) {
                bool found = false;
                foreach (var expected_cycle in expected_cycles) {
                    if (expected_cycle.Count != actual_cycle.Count)
                        continue;
                    bool same = true;
                    if (actual_cycle[1] == expected_cycle[1]) { // directions match
                        for (int i = 2; i < actual_cycle.Count - 1; ++i) {
                            same = actual_cycle[i] == expected_cycle[i];
                            if (!same)
                                break;
                        }
                    } else if (actual_cycle[1] == expected_cycle[expected_cycle.Count - 2]) { // directions don't match
                        for (int i = 2; i < expected_cycle.Count - 1; ++i) {
                            same = actual_cycle[i] == expected_cycle[expected_cycle.Count - i - 1];
                            if (!same)
                                break;
                        }
                    } else { // cycles don't match
                        same = false;
                    }
                    found = same;
                    if (found)
                        break;
                }
                Assert.True(found);
            }
        }
    }
}
