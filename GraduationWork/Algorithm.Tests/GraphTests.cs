﻿using System;
using System.Collections.Generic;
using Algorithms.Datastructures;
using Xunit;

namespace Algorithms.Tests {
    public class GraphTests {
        static private AdjacencyMatrix GetMatrixByNumber(int number) {
            AdjacencyMatrix matrix;
            switch (number) {
            case 1: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                    { false,  true,  true,  true }, // 0 1 1 1
                    {  true, false,  true,  true }, // 1 0 1 1
                    {  true,  true, false, false }, // 1 1 0 0
                    {  true,  true, false, false }  // 1 1 0 0
                });
            }
            break;
            case 2:
            default: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                    { false,  true,  true,  true, false }, // 0 1 1 1 0
                    {  true, false,  true,  true, false }, // 1 0 1 1 0
                    {  true,  true, false, false, true  }, // 1 1 0 0 1
                    {  true,  true, false, false, true  }, // 1 1 0 0 1
                    { false, false, true , true , false }  // 0 0 1 1 0
                });
            }
            break;
            }
            return matrix;
        }
        static private IncidencyLists GetListsByNumber(int number) {
            IncidencyLists lists;
            switch (number) {
            case 1: {
                lists = new IncidencyLists(new List<List<int>>()
                {
                    new List<int>() { 1, 2, 3 }, // 0: 1, 2, 3
                    new List<int>() { 0, 2, 3 }, // 1: 0, 2, 3
                    new List<int>() { 0, 1 },    // 2: 0, 1
                    new List<int>() { 0, 1 }     // 3: 0, 1
                });
            }
            break;
            case 2:
            default: {
                lists = new IncidencyLists(new List<List<int>>()
                {
                    new List<int>() { 1, 2, 3 },    // 0: 1, 2, 3
                    new List<int>() { 0, 2, 3 },    // 1: 0, 2, 3
                    new List<int>() { 0, 1, 4 },    // 2: 0, 1, 4
                    new List<int>() { 0, 1, 4 },    // 3: 0, 1, 4 
                    new List<int>() { 2, 3 }        // 4: 2, 3
                });
            }
            break;
            }
            return lists;
        }
        static private int GetOrderByNumber(int number) {
            int order;
            switch (number) {
            case 1:
            order = 4;
            break;
            case 2:
            default:
            order = 5;
            break;
            }
            return order;
        }
        static private int GetSizeByNumber(int number) {
            int size;
            switch (number) {
            case 1:
            size = 5;
            break;
            case 2:
            default:
            size = 7;
            break;
            }
            return size;
        }
        static private int[] GetDegreesByNumber(int number) {
            int[] degrees = null;
            switch (number) {
            case 1: {
                degrees = new int[4] { 3, 3, 2, 2 };
            }
            break;
            case 2:
            default: {
                degrees = new int[5] { 3, 3, 3, 3, 2 };
            }
            break;
            }
            return degrees;
        }

        #region Order & Size
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldCalculateOrderWithMatrixCorrectly(int number) {
            // Given:
            var givenGraph = new Graph(GetMatrixByNumber(number));
            // When:
            var expectedOrder = GetOrderByNumber(number);
            // Then:
            Assert.Equal(givenGraph.Order, expectedOrder);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldCalculateOrderWithListsCorrectly(int number) {
            // Given:
            var givenGraph = new Graph(GetListsByNumber(number));
            // When:
            var expectedOrder = GetOrderByNumber(number);
            // Then:
            Assert.Equal(givenGraph.Order, expectedOrder);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldCalculateSizeWithMatrixCorrectly(int number) {
            // Given:
            var givenGraph = new Graph(GetMatrixByNumber(number));
            // When:
            var expectedSize = GetSizeByNumber(number);
            // Then:
            Assert.Equal(expectedSize, givenGraph.Size);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldCalculateSizeWithListsCorrectly(int number) {
            // Given:
            var givenGraph = new Graph(GetListsByNumber(number));
            // When:
            var expectedSize = GetSizeByNumber(number);
            // Then:
            Assert.Equal(givenGraph.Size, expectedSize);
        }
        #endregion Order & Size

        #region Degree
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldCalculateDegreeCorrectly(int number) {
            // Given:
            var graph = new Graph(GetMatrixByNumber(number));
            var expected_degrees = GetDegreesByNumber(number);
            // When:
            var actual_degrees = graph.GetDegrees();
            // Then:
            Assert.Equal(expected_degrees, actual_degrees);
        }
        #endregion

        [Fact]
        void OrderAndSize_NullAssigment_ShouldFail() {
            /// action & assert
            Assert.Throws<ArgumentNullException>(() => new Graph(null as AdjacencyMatrix));
            Assert.Throws<ArgumentNullException>(() => new Graph(null as IncidencyLists));
            Graph graph = new Graph();
            Assert.Throws<ArgumentNullException>(() => graph.AsIncidencyLists = null);
            Assert.Throws<ArgumentNullException>(() => graph.AsIncidencyMatrix = null);
            Assert.Throws<ArgumentException>(() => graph.Order);
            Assert.Throws<ArgumentException>(() => graph.Size);
        }
    }
}
