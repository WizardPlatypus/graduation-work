﻿using System;
using Algorithms.Datastructures;
using Xunit;

namespace Algorithms.Tests {
    public class DijkstraTests {
        static void CheckIfNumberIsFine(int data_length, int number) {
            if (number < 0 || number >= data_length)
                throw new ArgumentOutOfRangeException(nameof(number), $"Must be not negative and less than data length\nNumber: {number}, Data length: {data_length}");
        }
        static WeightedMatrix GetMatrixByNumber(int number) {
            var data = new WeightedMatrix[]
            {
                new WeightedMatrix(6, new int[,]
                {
                    {  0,  7,  9,  0,  0, 14 },
                    {  7,  0, 10, 15,  0,  0 },
                    {  9, 10,  0, 11,  0,  2 },
                    {  0, 15, 11,  0,  6,  0 },
                    {  0,  0,  0,  6,  0,  9 },
                    { 14,  0,  2,  0,  9,  0 }
                }),
            };
            if (number < 0 || number >= data.Length)
                throw new ArgumentOutOfRangeException(nameof(number), $"Must be not negative and less than data length\nNumber: {number}, Data length: {data.Length}");
            return data[number];
        }
        static int GetStartVertexByNumber(int number) {
            var data = new int[] { 0 };
            CheckIfNumberIsFine(data.Length, number);
            return data[number];
        }
        static int[] GetDistancesByNumber(int number) {
            var data = new int[][]
            {
                new int[] { 0, 7, 9, 20, 20, 11 },
            };
            CheckIfNumberIsFine(data.Length, number);
            return data[number];
        }
        static int[] GetSequenceByNumber(int number) {
            var data = new int[][]
            {
                new int[] { 0, 1, 2, 5, 3, 4 },
            };
            CheckIfNumberIsFine(data.Length, number);
            return data[number];
        }
        static int[] GetAncestorsByNumber(int number) {
            var data = new int[][]
            {
                new int[] { -1, 0, 0, 2, 5, 2 },
            };
            CheckIfNumberIsFine(data.Length, number);
            return data[number];
        }

        [Theory]
        [InlineData(0)]
        public void DistancesShouldBeCalculatedCorrectly(int number) {
            // Given :
            var algorithm = new Dijkstra();
            var weighted_matrix = GetMatrixByNumber(number);
            var start_vertex = GetStartVertexByNumber(number);
            var expected_distances = GetDistancesByNumber(number);
            // When :
            algorithm.SetInput(new Dijkstra.Input(weighted_matrix, start_vertex));
            algorithm.Start();
            var result = algorithm.GetResult() as Dijkstra.Result;
            // Then :
            Assert.Equal(expected_distances, result.Distances);
        }
        [Theory]
        [InlineData(0)]
        public void SequenceShouldBeCreatedCorrectly(int number) {
            // Given :
            var algorithm = new Dijkstra();
            var weighted_matrix = GetMatrixByNumber(number);
            var start_vertex = GetStartVertexByNumber(number);
            var expected_sequence = GetSequenceByNumber(number);
            // When :
            algorithm.SetInput(new Dijkstra.Input(weighted_matrix, start_vertex));
            algorithm.Start();
            var result = algorithm.GetResult() as Dijkstra.Result;
            // Then :
            Assert.Equal(expected_sequence, result.Sequence);
        }
        [Theory]
        [InlineData(0)]
        public void AncestorsShouldBeCalculatedCorrectly(int number) {
            // Given :
            var algorithm = new Dijkstra();
            var weighted_matrix = GetMatrixByNumber(number);
            var start_vertex = GetStartVertexByNumber(number);
            var expected_ancestors = GetAncestorsByNumber(number);
            // When :
            algorithm.SetInput(new Dijkstra.Input(weighted_matrix, start_vertex));
            algorithm.Start();
            var result = algorithm.GetResult() as Dijkstra.Result;
            // Then :
            Assert.Equal(expected_ancestors, result.Ancestors);
        }
    }
}
