﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms.Datastructures;

using Xunit;

namespace Algorithms.Tests {
    public class TwoSalesmenTests {
        const int NUMBER_OF_TEST_CASES = 2;
        #region Test Data
        static int GetTargetVertexByNumber(int number) {
            int target_vertex = -1;
            switch (number) {
            case 0:
            default: {
                target_vertex = 0;
            }
            break;
            case 1: {
                target_vertex = 4;
            }
            break;
            }
            return target_vertex;
        }

        #region Weighted Matrices
        static WeightedMatrix[] _weightedMatrices = new WeightedMatrix[NUMBER_OF_TEST_CASES];
        static WeightedMatrix GetWeightedMatrixByNumber(int number) {
            if (_weightedMatrices[number] == null) {
                switch (number) {
                case 0:
                default: {
                    _weightedMatrices[number] =
                      new WeightedMatrix(5, new int[,] {
                                    {  0, 12,  5,  4,  0 },
                                    { 12,  0, 19,  0,  0 },
                                    {  5, 19,  0, 10, 19 },
                                    {  4,  0, 10,  0,  5 },
                                    {  0,  0, 19,  5,  0 },
                        });
                }
                break;
                case 1: {
                    _weightedMatrices[number] =
                        new WeightedMatrix(5, new int[,]
                        {
                                    {  0,  4,  8,  0, 12 },
                                    {  4,  0, 12,  0,  6 },
                                    {  8, 12,  0,  4,  0 },
                                    {  0,  0,  4,  0, 10 },
                                    { 12,  6,  0, 10,  0 }
                        });
                }
                break;
                }
            }
            return _weightedMatrices[number];
        }
        #endregion Weighted Matrices

        #region Results
        static TwoSalesmen.Result[] _results
            = new TwoSalesmen.Result[NUMBER_OF_TEST_CASES];
        static TwoSalesmen.Result GetResultByNumber(int number) {
            if (_results[number] == null) {
                var algorithm = new TwoSalesmen();
                algorithm.SetInput(new TwoSalesmen.Input(
                    GetWeightedMatrixByNumber(number),
                    GetTargetVertexByNumber(number)));
                algorithm.Start();
                _results[number] = algorithm.GetResult() as TwoSalesmen.Result;
            }
            return _results[number];
        }
        #endregion RESULTS

        #region Actual Cycles
        static List<List<int>> GetActualCyclesByNumber(int number) {
            List<List<int>> cycles = new List<List<int>>();
            var result = GetResultByNumber(number);
            cycles.Add(result.Cycle1);
            cycles.Add(result.Cycle2);
            return cycles;
        }
        #endregion Actual Cycles

        #region Expected Cycles
        static List<List<int>>[] _expectedCycles = new List<List<int>>[NUMBER_OF_TEST_CASES];
        static List<List<int>> GetExpectedCyclesByNumber(int number) {
            if (_expectedCycles[number] == null) {
                // Calculate _expectedCycles for number
                switch (number) {
                case 0:
                default: {
                    _expectedCycles[number] = new List<List<int>>() {
                                    new List<int>() { 0, 1, 2, 0 },
                                    new List<int>() { 0, 2, 4, 3, 0 },
                                };
                }
                break;
                case 1: {
                    _expectedCycles[number] = new List<List<int>>()
                    {
                                new List<int>() { 4, 1, 0, 2, 3, 4 },
                                null
                            };
                }
                break;
                }
            }
            return _expectedCycles[number];
        }
        #endregion Expected Cycles

        #region Weights
        static List<int>[] _actualWeights = new List<int>[NUMBER_OF_TEST_CASES];
        static List<int> GetActualWeightsByNumber(int number) {
            if (_actualWeights[number] == null) {
                var result = GetResultByNumber(number);
                _actualWeights[number] = new List<int>(2)
                {
                    result.Weight1,
                    result.Weight2
                };
            }
            return _actualWeights[number];
        }
        static List<int>[] _expectedWeights = new List<int>[NUMBER_OF_TEST_CASES];
        static List<int> GetExpectedWeightsByNumber(int number) {
            if (_expectedWeights[number] == null) {
                switch (number) {
                case 0:
                default: {
                    _expectedWeights[number] = new List<int>() { 36, 33 };
                }
                break;
                case 1: {
                    _expectedWeights[number] = new List<int>() { 32, 0 };
                }
                break;
                }
            }
            return _expectedWeights[number];
        }
        #endregion Weights

        #endregion Test Data

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        void ShouldCalculateCyclesCorrectly(int number) {
            // When given: actual cycles, expected cycles
            var actual_cycles = GetActualCyclesByNumber(number);
            var expected_cycles = GetExpectedCyclesByNumber(number);
            // Then: cycles must match
            bool match = (AreEqual(actual_cycles[0], expected_cycles[0])
                && AreEqual(actual_cycles[1], expected_cycles[1])) ||
                (AreEqual(actual_cycles[0], expected_cycles[1])
                && AreEqual(actual_cycles[1], expected_cycles[0]));
            Assert.True(match);
        }
        static bool AreEqual(List<int> cycle1, List<int> cycle2) {
            if (cycle1 == null && cycle2 != null)
                return false;
            if (cycle2 == null && cycle1 != null)
                return false;
            if (cycle1 == null && cycle2 == null)
                return true;
            if (cycle1.Count != cycle2.Count)
                return false;
            return cycle1.SequenceEqual(cycle2) || cycle1.SequenceEqual((cycle2 as IEnumerable<int>).Reverse());
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        void ShouldCalcualteWeightsCorrectly(int number) {
            // When given: actual weight1, weight2; expected weight1, weight2
            var actual_weights = GetActualWeightsByNumber(number);
            var expected_weights = GetExpectedWeightsByNumber(number);
            // Then: weights must match
            bool match = (actual_weights[0] == expected_weights[0] && actual_weights[1] == expected_weights[1])
                      || (actual_weights[0] == expected_weights[1] && actual_weights[1] == expected_weights[0]);
            Assert.True(match, $"actual1: {actual_weights[0]}; actual2: {actual_weights[1]}; expected1: {expected_weights[0]}; expected2: {expected_weights[1]}.");
        }
    }
}
