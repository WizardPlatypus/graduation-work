﻿using System.Collections.Generic;

using Algorithms.Multigraph;
using Xunit;

namespace Algorithms.Tests {
    public class MultimatrixTests {
        [Fact]
        void MatrixShouldBeInitializedWithNull() {
            // When given:
            Multimatrix multimatrix = new Multimatrix(size: 1, order: 1);
            // Then:
            Assert.Null(multimatrix[0, 0]);
        }
        [Fact]
        void MatrixShouldBeSymetrical() {
            // Given:
            Multimatrix multimatrix = new Multimatrix(2, 2);
            var data = new List<int> { 2, 4 };
            int first = 0, second = 1;
            // When:
            multimatrix[first, second] = data;
            // Then:
            Assert.Equal(data, multimatrix[second, first]);
        }
    }
}
