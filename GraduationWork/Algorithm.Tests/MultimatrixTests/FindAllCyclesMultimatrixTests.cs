﻿using System.Collections.Generic;
using Algorithms.Datastructures;
using Xunit;
using Algorithms.Multigraph;

namespace Algorithms.Tests {
    public class FindAllCyclesMultimatrixTests {
        AdjacencyMatrix GetMatrixByNumber(int number) {
            AdjacencyMatrix matrix = null;
            switch (number) {
            case 0:
            default: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                            { false,  true,  true,  true, false }, // 0 1 1 1 0
                            {  true, false,  true, false, false }, // 1 0 1 0 0
                            {  true,  true, false,  true,  true }, // 1 1 0 1 1
                            {  true, false,  true, false,  true }, // 1 0 1 0 1
                            { false, false,  true,  true, false }, // 0 0 1 1 0
                });
            }
            break;
            }
            return matrix;
        }
        Multimatrix GetMultimatrixAndEdgesFromMatrix(AdjacencyMatrix matrix) {
            var algorithm = new GraphToMultimatrix();
            algorithm.SetInput(new GraphToMultimatrix.Input(new Graph(matrix)));
            algorithm.Start();
            var result = algorithm.GetResult() as GraphToMultimatrix.Result;
            return result.Multimatrix;
        }
        List<int[]> GetCyclesByNumber(int number) {
            List<int[]> cycles = null;
            switch (number) {
            case 0:
            default: {
                cycles = new List<int[]>
                {
                            new int[] { 0, 1, 2, 0 },
                            new int[] { 0, 1, 2, 3, 0 },
                            new int[] { 0, 1, 2, 3, 4, 2, 0 },
                            new int[] { 0, 1, 2, 4, 3, 0 },
                            new int[] { 0, 1, 2, 4, 3, 2, 0 },
                            new int[] { 0, 2, 3, 0 },
                            new int[] { 0, 2, 4, 3, 0 },
                        };
            }
            break;
            }
            return cycles;
        }
        List<List<int>> GetMulticyclesByNumber(int number) {
            List<List<int>> multicycles = null;
            switch (number) {
            case 0:
            default: {
                multicycles = new List<List<int>>()
                {
                            new List<int>() { 0, 0, 1, 1, 0 },
                            new List<int>() { 0, 0, 1, 3, 2, 2, 0 },
                            new List<int>() { 0, 0, 1, 3, 2, 4, 1, 1, 0 },
                            new List<int>() { 0, 0, 1, 4, 2, 2, 0 },
                            new List<int>() { 0, 0, 1, 4, 2, 3, 1, 1, 0},
                            new List<int>() { 0, 1, 1, 3, 2, 2, 0 },
                            new List<int>() { 0, 1, 1, 4, 2, 2, 0 }
                        };
            }
            break;
            }
            return multicycles;
        }
        int GetStartVertexByNumber(int number) {
            int start_vertex = 0;
            switch (number) {
            case 0:
            default: {
                start_vertex = 0;
            }
            break;
            }
            return start_vertex;
        }
        bool CyclesAreValid(List<List<int>> cycles, int graph_order) {
            foreach (var cycle in cycles) {
                bool[,] visited = new bool[graph_order, graph_order];
                for (int i = 0; i < cycle.Count - 1; ++i) {
                    if (visited[cycle[i], cycle[i + 1]])
                        return false;
                    visited[cycle[i], cycle[i + 1]] = true;
                }
            }
            return true;
        }
        FindAllCyclesMultimatrix.Result GetResultByNumber(int number) {
            // Given:
            Multimatrix multimatrix = GetMultimatrixAndEdgesFromMatrix(GetMatrixByNumber(number));
            var start_vertex = GetStartVertexByNumber(number);
            // When:
            var algorithm = new FindAllCyclesMultimatrix();
            algorithm.SetInput(new FindAllCyclesMultimatrix.Input(multimatrix, start_vertex));
            algorithm.Start();
            return algorithm.GetResult() as FindAllCyclesMultimatrix.Result;
        }

        [Theory]
        [InlineData(0)]
        void CyclesShouldBeValid(int number) {
            // Given:
            var result = GetResultByNumber(number);
            var multimatrix = GetMultimatrixAndEdgesFromMatrix(GetMatrixByNumber(number));
            // Then:
            foreach (var cycle in result.Cycles) {
                Assert.True(cycle.Count % 2 == 1);
                bool[] used = new bool[multimatrix.Size];
                for (int i = 1; i < cycle.Count; i += 2) {
                    var edge = cycle[i];
                    Assert.False(used[edge]);
                    used[edge] = true;
                }
            }
        }
        [Theory]
        [InlineData(0)]
        void ShouldFindCyclesCorrectlyWithMatrix(int number) {
            // Given:
            var expected_cycles = GetMulticyclesByNumber(number);
            var result = GetResultByNumber(number);
            // Then:
            Assert.True(expected_cycles.Count == result.Cycles.Count, "There is different number of cycles");
            foreach (var expected_cycle in expected_cycles) {
                bool found = false;
                foreach (var actual_cycle in result.Cycles) {
                    if (expected_cycle.Count != actual_cycle.Count)
                        continue;
                    bool same = true;
                    for (int i = 0; i < expected_cycle.Count; ++i) {
                        same = expected_cycle[i] == actual_cycle[i];
                        if (!same)
                            break;
                    }
                    found = same;
                    if (found)
                        break;
                }
                Assert.True(found);
            }
        }
    }
}
