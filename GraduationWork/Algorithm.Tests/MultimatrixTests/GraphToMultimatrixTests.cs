﻿using System.Collections.Generic;
using Algorithms.Datastructures;
using Algorithms.Multigraph;
using Xunit;

namespace Algorithms.Tests {
    public class GraphToMultimatrixTests {
        AdjacencyMatrix GetMatrixByNumber(int number) {
            AdjacencyMatrix matrix = null;
            switch (number) {
            case 0:
            default: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                            { false,  true,  true, false,  true }, // 0 1 1 0 1
                            {  true, false,  true, false, false }, // 1 0 1 0 0
                            {  true,  true, false,  true, false }, // 1 1 0 1 0
                            { false, false,  true, false,  true }, // 0 0 1 0 1
                            {  true, false, false,  true, false }, // 1 0 0 1 0
                });
            }
            break;
            }
            return matrix;
        }
        Multimatrix GetMultimatrixByNumber(int number) {
            Multimatrix multimatrix = null;
            switch (number) {
            case 0:
            default: {
                multimatrix = new Multimatrix(order: 2, size: 3);
                multimatrix[0, 1] = new List<int> { 1, 2, 3 };
            }
            break;
            }
            return multimatrix;
        }

        List<List<int>> GetEdgesByNumber(int number) {
            List<List<int>> edges = null;
            switch (number) {
            case 0:
            default: {
                edges = new List<List<int>>()
                {
                            new List<int>() { 2 },
                            new List<int>() {},
                            new List<int>() { 4, 5 }
                        };
            }
            break;
            }
            return edges;
        }

        [Theory]
        [InlineData(0)]
        void ShouldTransformMatrixToMultimatrixCorrectly(int number) {
            // Given:
            var matrix = GetMatrixByNumber(number);
            var expected_multimatrix = GetMultimatrixByNumber(number);
            var graph = new Graph(matrix);
            var algorithm = new GraphToMultimatrix();
            // When:
            algorithm.SetInput(new GraphToMultimatrix.Input(graph));
            algorithm.Start();
            var result = algorithm.GetResult() as GraphToMultimatrix.Result;
            // Then:
            Assert.Equal(expected_multimatrix.Order, result.Multimatrix.Order);
            for (int vertex1 = 0; vertex1 < expected_multimatrix.Order; ++vertex1) {
                for (int vertex2 = vertex1 + 1; vertex2 < result.Multimatrix.Order; ++vertex2) {
                    if (expected_multimatrix[vertex1, vertex2] == null)
                        Assert.Null(result.Multimatrix[vertex1, vertex2]);
                    else
                        Assert.Equal(
                          expected_multimatrix[vertex1, vertex2].Count,
                          result.Multimatrix[vertex1, vertex2].Count
                        );
                }
            }
        }
        [Theory]
        [InlineData(0)]
        void ShouldCalculateEdgesCorrectly(int number) {
            // Given:
            var matrix = GetMatrixByNumber(number);
            var expected_edges = GetEdgesByNumber(number);
            var graph = new Graph(matrix);
            var algorithm = new GraphToMultimatrix();
            // When:
            algorithm.SetInput(new GraphToMultimatrix.Input(graph));
            algorithm.Start();
            var result = algorithm.GetResult() as GraphToMultimatrix.Result;
            // Then:
            // actual number of edges must be equal to the expected number 
            Assert.Equal(expected_edges.Count, result.Edges.Count);
            // each expected edge must have respective actual edge
            for (int i = 0; i < expected_edges.Count; ++i) {
                bool found = false;
                for (int j = 0; j < result.Edges.Count; ++j) {
                    found = expected_edges[i].Count == result.Edges[j].Count;
                    if (found) {
                        result.Edges.RemoveAt(j);
                        break;
                    }
                }
                Assert.True(found);
            }
        }
    }
}
