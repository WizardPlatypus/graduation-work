﻿using System.Collections.Generic;
using Algorithms.Datastructures;

using Xunit;

namespace Algorithms.Tests {
    public class ToWeightCyclesTests {
        static List<List<int>> GetCycles(int number) {
            List<List<int>> cycles = null;
            switch (number) {
            case 0:
            default: {
                cycles = new List<List<int>>
                {
                            new List<int>() { 0, 1, 2, 0 },
                            new List<int>() { 0, 1, 2, 3, 0 },
                            new List<int>() { 0, 1, 2, 3, 4, 2, 0 },
                            new List<int>() { 0, 1, 2, 4, 3, 0 },
                            new List<int>() { 0, 1, 2, 4, 3, 2, 0 },
                            new List<int>() { 0, 2, 3, 0 },
                            new List<int>() { 0, 2, 4, 3, 0 },
                        };
            }
            break;
            }
            return cycles;
        }
        static WeightedMatrix GetWeightedMatrix(int number) {
            WeightedMatrix matrix = null;
            switch (number) {
            case 0:
            default: {
                matrix = new WeightedMatrix(5, new int[,]
                {
                            {  0, 12,  5,  4,  0 },
                            { 12,  0, 19,  0,  0 },
                            {  5, 19,  0, 10, 19 },
                            {  4,  0, 10,  0,  5 },
                            {  0,  0, 19,  5,  0 },
                });
            }
            break;
            }
            return matrix;
        }
        List<int> GetExpectedWeights(int number) {
            List<int> weights = null;
            switch (number) {
            case 0:
            default: { // fill expected weights
                weights = new List<int>() { 36, 45, 70, 59, 70, 19, 33 };
            }
            break;
            }
            return weights;
        }
        List<int> GetActualWeights(int number) {
            var cycles = GetCycles(number);
            var weightedMatrix = GetWeightedMatrix(number);
            var algorithm = new WeightCycles();
            algorithm.SetInput(new WeightCycles.Input(cycles, weightedMatrix));
            algorithm.Start();
            var result = algorithm.GetResult() as WeightCycles.Result;
            return result.Weights;
        }

        [Theory]
        [InlineData(0)]
        void ShouldCalculatesWeightsCorrectly(int number) {
            // When given: actual weights, expected weights
            var actual_weights = GetActualWeights(number);
            var expected_weights = GetExpectedWeights(number);
            // Then: must be equal
            Assert.Equal(expected_weights, actual_weights);
        }
    }
}
