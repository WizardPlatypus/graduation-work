﻿using System;
using System.Collections.Generic;
using Algorithms.Datastructures;
using Xunit;

namespace Algorithms.Tests {
    public class BreadthFirstSearchTests {
        int GetMaxDepthByNumber(int number) {
            int maxDepth;
            switch (number) {
            case 1: {
                maxDepth = 2;
            }
            break;
            case 2: {
                maxDepth = 2;
            }
            break;
            default: {
                throw new NotImplementedException();
            }
            }
            return maxDepth;
        }
        int[] GetSequenceByNumber(int number) {
            int[] sequence;
            switch (number) {
            case 1: {
                sequence = new int[] { 2, 0, 4, 5, 1, 3 };
            }
            break;
            case 2: {
                sequence = new int[] { 5, 1, 2, 4, 3, 0 };
            }
            break;
            default: {
                throw new NotImplementedException();
            }
            }
            return sequence;
        }
        int[] GetDistancesByNumber(int number) {
            int[] distances;
            switch (number) {
            case 1: {
                distances = new int[] { 1, 2, 0, 2, 1, 1 };
            }
            break;
            case 2: {
                distances = new int[] { 2, 1, 1, 2, 1, 0 };
            }
            break;
            default: {
                throw new NotImplementedException();
            }
            }
            return distances;
        }
        int GetStartingVertexByNumber(int number) {
            int vertex;
            switch (number) {
            case 1: {
                vertex = 2;
            }
            break;
            case 2: {
                vertex = 5;
            }
            break;
            default:
            throw new NotImplementedException();
            }
            return vertex;
        }
        IncidencyLists GetIncidencyListsByNumber(int number) {
            IncidencyLists lists;
            switch (number) {
            case 1: {
                lists = new IncidencyLists(new List<List<int>>()
                {
                            new List<int>{ 1, 2, 4 }, // 0: 1, 2, 4
                            new List<int>{ 0, 3 },    // 1: 0, 3
                            new List<int>{ 0, 4, 5 }, // 2: 0, 4, 5
                            new List<int>{ 1, 4 },    // 3: 1, 4
                            new List<int>{ 0, 2, 3 }, // 4: 0, 2, 3
                            new List<int>{ 2 },       // 5: 2
                        });
            }
            break;
            case 2:
            default: {
                lists = new IncidencyLists(new List<List<int>>() {
                            new List<int>{ 4 },       // 0: 4
                            new List<int>{ 3, 4, 5 }, // 1: 3, 4, 5
                            new List<int>{ 3, 5 },    // 2: 3, 5
                            new List<int>{ 1, 2 },    // 3: 1, 2
                            new List<int>{ 0, 1, 5 }, // 4: 0, 1, 5
                            new List<int>{ 1, 2, 4 }, // 5: 1, 2, 4
                        });
            }
            break;
            }
            return lists;
        }
        AdjacencyMatrix GetIncidencyMatrixByNumber(int number) {
            AdjacencyMatrix matrix;
            switch (number) {
            case 1: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                            { false,  true,  true, false,  true, false }, // 0 1 1 0 1 0
                            {  true, false, false,  true, false, false }, // 1 0 0 1 0 0
                            {  true, false, false, false,  true,  true }, // 1 0 0 0 1 1
                            { false,  true, false, false,  true, false }, // 0 1 0 0 1 0
                            {  true, false,  true,  true, false, false }, // 1 0 1 1 0 0
                            { false, false,  true, false, false, false }, // 0 0 1 0 0 0
                });
            }
            break;
            case 2: {
                matrix = new AdjacencyMatrix(new bool[,]
                {
                            { false, false, false, false,  true, false }, // 0 0 0 0 1 0
                            { false, false, false,  true,  true,  true }, // 0 0 0 1 1 1
                            { false, false, false,  true, false,  true }, // 0 0 0 1 0 1
                            { false,  true,  true, false, false, false }, // 0 1 1 0 0 0
                            {  true,  true, false, false, false,  true }, // 1 1 0 0 0 1
                            { false,  true,  true, false,  true, false }, // 0 1 1 0 1 0
                });
            }
            break;
            default: { throw new NotImplementedException(); }
            }
            return matrix;
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_MaxDepth_IncidencyMatrix_RecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyMatrixByNumber(number));
            var algorithm = new RecBFSbyIncidencyMatrix();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetMaxDepthByNumber(number), result.MaxDepth);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_MaxDepth_IncidencyMatrix_NonRecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyMatrixByNumber(number));
            var algorithm = new NonRecBFSbyIncidencyMatrix();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetMaxDepthByNumber(number), result.MaxDepth);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Distances_IncidencyMatrix_RecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyMatrixByNumber(number));
            var algorithm = new RecBFSbyIncidencyMatrix();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetDistancesByNumber(number), result.Distances);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Distances_IncidencyMatrix_NonRecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyMatrixByNumber(number));
            var algorithm = new NonRecBFSbyIncidencyMatrix();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetDistancesByNumber(number), result.Distances);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Sequence_IncidencyMatrix_RecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyMatrixByNumber(number));
            var algorithm = new RecBFSbyIncidencyMatrix();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetSequenceByNumber(number), result.Sequence);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Sequence_IncidencyMatrix_NonRecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyMatrixByNumber(number));
            var algorithm = new NonRecBFSbyIncidencyMatrix();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetSequenceByNumber(number), result.Sequence);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_MaxDepth_IncidencyLists_RecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyListsByNumber(number));
            var algorithm = new RecBFSbyIncidencyLists();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetMaxDepthByNumber(number), result.MaxDepth);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_MaxDepth_IncidencyLists_NonRecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyListsByNumber(number));
            var algorithm = new NonRecBFSbyIncidencyLists();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetMaxDepthByNumber(number), result.MaxDepth);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Distances_IncidencyLists_RecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyListsByNumber(number));
            var algorithm = new RecBFSbyIncidencyLists();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetDistancesByNumber(number), result.Distances);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Distances_IncidencyLists_NonRecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyListsByNumber(number));
            var algorithm = new NonRecBFSbyIncidencyLists();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetDistancesByNumber(number), result.Distances);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Sequence_IncidencyLists_RecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyListsByNumber(number));
            var algorithm = new RecBFSbyIncidencyLists();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetSequenceByNumber(number), result.Sequence);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void ShouldCalculateCorrectly_Sequence_IncidencyLists_NonRecAlgorithm(int number) {
            // Given:
            var graph = new Graph(GetIncidencyListsByNumber(number));
            var algorithm = new NonRecBFSbyIncidencyLists();
            // When:
            algorithm.SetInput(new BFS.Input(graph, GetStartingVertexByNumber(number)));
            algorithm.Start();
            var result = algorithm.GetResult() as BFS.Result;
            // Then:
            Assert.Equal(GetSequenceByNumber(number), result.Sequence);
        }
    }
}
