﻿using System.Collections.Generic;
using Algorithms.Datastructures;

using Xunit;

namespace Algorithms.Tests {
    public class FlatMatrixTests {
        FlatMatrix<int> GetWeightedMatrixByNumber(int number) {
            FlatMatrix<int> matrix = null;
            switch (number) {
            case 0:
            default: {
                matrix = new FlatMatrix<int>(3, new int[,]
                {
                            { 0, 2, 1 },
                            { 2, 0, 0 },
                            { 1, 0, 0 },
                });
            }
            break;
            case 1: {
                matrix = new FlatMatrix<int>(5, new int[,]
                {
                            { 0, 1, 2, 3, 4 },
                            { 0, 0, 5, 6, 7 },
                            { 0, 0, 0, 8, 9 },
                            { 0, 0, 0, 0, 10 },
                            { 0, 0, 0, 0, 0 },
                });
            }
            break;
            case 2: {
                matrix = new FlatMatrix<int>(4, new int[,]
                {
                            { 0, 1, 2, 3 },
                            { 0, 0, 4, 5 },
                            { 0, 0, 0, 6 },
                            { 0, 0, 0, 0 },
                });
            }
            break;
            }
            return matrix;
        }
        int GetDeletionVictimByNumber(int number) {
            int victim;
            switch (number) {
            case 1:
            victim = 4;
            break;
            default:
            case 2:
            victim = 1;
            break;
            }
            return victim;
        }
        List<int> GetExpectedValuesAfterDeletionByNumber(int number) {
            List<int> values;
            switch (number) {
            case 1:
            values = new List<int> { 1, 2, 3, 5, 6, 8 };
            break;
            default:
            case 2:
            values = new List<int> { 2, 3, 6 };
            break;
            }
            return values;
        }
        List<int> GetExpectedValuesAfterGrowthByNumber(int number) {
            List<int> values;
            switch (number) {
            case 1:
            values = new List<int> { 1, 2, 3, 4, 0, 5, 6, 7, 0, 8, 9, 0, 10, 0, 0 };
            break;
            default:
            case 2:
            values = new List<int> { 1, 2, 3, 0, 0, 4, 5, 0, 0, 6, 0, 0, 0, 0, 0 };
            break;
            }
            return values;
        }
        int GetGrowthValueByNumber(int number) {
            int value;
            switch (number) {
            case 1:
            value = 1;
            break;
            default:
            case 2:
            value = 2;
            break;
            }
            return value;
        }

        /*
        [Theory]
        [InlineData(0)]
        void ShouldTransformToMatrixCorrectly(int number)
        {
            // Given:
            var weightedMatrix = GetWeightedMatrixByNumber(number);
            // When:
            var matrix = (weightedMatrix as WeightedMatrix).AsMatrix;
            // Then:
            Assert.Equal(weightedMatrix.Size, matrix.Size);
            for (int i = 0; i < weightedMatrix.Size; ++i)
            {
                for (int j = 0; j < weightedMatrix.Size; ++j)
                {
                    if (weightedMatrix[i, j] == 0)
                        Assert.False(matrix[i, j]);
                    else
                        Assert.True(matrix[i, j]);
                }
            }
        }
        */
        [Theory]
        [InlineData(4)]
        void ShouldNotGoUnderMinDegrees(int order, int min_degree = 2, int max_weight = 10, int tests = 10) {
            // Given
            while (tests-- != 0) {
                // When
                var matrix = WeightedMatrix.GetRandom(order, max_weight, min_degree);
                var graph = new Graph(matrix.AsMatrix);
                // Then
                foreach (var degree in graph.GetDegrees()) {
                    Assert.True(degree >= min_degree);
                }
            }
        }
        [Theory]
        [InlineData(5, 8)]
        [InlineData(9, 25)]
        void ShouldGenerateWithCorrectDegrees(int order, int size, int min_degree = 2, int tests = 25, int max_weight = 15) {
            for (int test = 0; test < tests; test++) {
                // When
                var matrix = WeightedMatrix.GetRandom(order, size, max_weight, min_degree);
                var degrees = new Graph(matrix.AsMatrix).GetDegrees();
                // Then
                foreach (var degree in degrees) {
                    Assert.True(degree >= min_degree);
                }
            }
        }
        [Theory]
        [InlineData(5, 8)]
        [InlineData(9, 25)]
        void ShouldGenerateWithCorrectOrder(int order, int size, int min_degree = 2, int tests = 25, int max_weight = 15) {
            for (int test = 0; test < tests; test++) {
                // When
                var matrix = WeightedMatrix.GetRandom(order, size, max_weight, min_degree);
                // Then
                Assert.True(matrix.Size == order);
            }
        }
        [Theory]
        [InlineData(5, 8)]
        [InlineData(9, 25)]
        void ShouldGenerateWithCorrectSize(int order, int size, int min_degree = 2, int tests = 25, int max_weight = 15) {
            for (int test = 0; test < tests; test++) {
                // When
                var matrix = WeightedMatrix.GetRandom(order, size, max_weight, min_degree);
                // Then
                int count = 0;
                foreach (var edge in matrix.GetValues()) {
                    if (edge == 0)
                        continue;
                    count++;
                }
                Assert.True(count == size);
            }
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldDeleteVerticesCorrectly(int number) {
            // Given:
            var matrix = GetWeightedMatrixByNumber(number);
            var victim = GetDeletionVictimByNumber(number);
            var expected_values = GetExpectedValuesAfterDeletionByNumber(number);
            // When:
            matrix.Delete(victim);
            // Then:
            Assert.Equal(expected_values, matrix.GetValues());
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldAddVerticesCorrectly(int number) {
            // Given:
            var matrix = GetWeightedMatrixByNumber(number);
            var expected_vales = GetExpectedValuesAfterGrowthByNumber(number);
            var growth_value = GetGrowthValueByNumber(number);
            // When:
            matrix.Grow(growth_value);
            // Then:
            Assert.Equal(expected_vales, matrix.GetValues());
        }
    }
}
