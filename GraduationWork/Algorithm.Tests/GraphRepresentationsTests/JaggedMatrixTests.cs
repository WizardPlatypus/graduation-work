﻿using System.Collections.Generic;
using Xunit;
using Algorithms.Datastructures;

namespace Algorithms.Tests.GraphRepresentationsTests {
    public class JaggedMatrixTests {
        JaggedMatrix<int> GetWeightedMatrixByNumber(int number) {
            JaggedMatrix<int> matrix = null;
            switch (number) {
            case 0:
            default: {
                matrix = new JaggedMatrix<int>(3, new int[,]
                {
                            { 0, 2, 1 },
                            { 2, 0, 0 },
                            { 1, 0, 0 },
                });
            }
            break;
            case 1: {
                matrix = new JaggedMatrix<int>(5, new int[,]
                {
                            { 0, 1, 2, 3, 4 },
                            { 0, 0, 5, 6, 7 },
                            { 0, 0, 0, 8, 9 },
                            { 0, 0, 0, 0, 10 },
                            { 0, 0, 0, 0, 0 },
                });
            }
            break;
            case 2: {
                matrix = new JaggedMatrix<int>(4, new int[,]
                {
                            { 0, 1, 2, 3 },
                            { 0, 0, 4, 5 },
                            { 0, 0, 0, 6 },
                            { 0, 0, 0, 0 },
                });
            }
            break;
            }
            return matrix;
        }
        int GetDeletionVictimByNumber(int number) {
            int victim;
            switch (number) {
            case 1:
            victim = 4;
            break;
            default:
            case 2:
            victim = 1;
            break;
            }
            return victim;
        }
        List<int> GetExpectedValuesAfterDeletionByNumber(int number) {
            List<int> values;
            switch (number) {
            case 1:
            values = new List<int> { 1, 2, 3, 5, 6, 8 };
            break;
            default:
            case 2:
            values = new List<int> { 2, 3, 6 };
            break;
            }
            return values;
        }
        List<int> GetExpectedValuesAfterGrowthByNumber(int number) {
            List<int> values;
            switch (number) {
            case 1:
            values = new List<int> { 1, 2, 3, 4, 0, 5, 6, 7, 0, 8, 9, 0, 10, 0, 0 };
            break;
            default:
            case 2:
            values = new List<int> { 1, 2, 3, 0, 0, 4, 5, 0, 0, 6, 0, 0, 0, 0, 0 };
            break;
            }
            return values;
        }
        int GetGrowthValueByNumber(int number) {
            int value;
            switch (number) {
            case 1:
            value = 1;
            break;
            default:
            case 2:
            value = 2;
            break;
            }
            return value;
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldDeleteVerticesCorrectly(int number) {
            // Given:
            var matrix = GetWeightedMatrixByNumber(number);
            var victim = GetDeletionVictimByNumber(number);
            var expected_values = GetExpectedValuesAfterDeletionByNumber(number);
            // When:
            matrix.Delete(victim);
            // Then:
            Assert.Equal(expected_values, matrix.GetValues());
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        void ShouldAddVerticesCorrectly(int number) {
            // Given:
            var matrix = GetWeightedMatrixByNumber(number);
            var expected_vales = GetExpectedValuesAfterGrowthByNumber(number);
            var growth_value = GetGrowthValueByNumber(number);
            // When:
            matrix.Grow(growth_value);
            // Then:
            Assert.Equal(expected_vales, matrix.GetValues());
        }
    }
}
